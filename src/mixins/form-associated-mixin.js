import { DelegateFocusMixin } from './delegate-focus-mixin.js';

export const supportsElementInternals =
  'ElementInternals' in window &&
  'setFormValue' in window.ElementInternals.prototype;

/**
 * Base function for providing Custom Element Form Association.
 *
 * @link https://web.dev/more-capable-form-controls
 * @link https://html.spec.whatwg.org/multipage/custom-elements.html#the-elementinternals-interface
 */
export const FormAssociatedMixin = superClass =>
  class extends DelegateFocusMixin(superClass) {
    /**
     * Adding a static formAssociated property, with a true value,
     * makes an autonomous custom element a form-associated custom element.
     *
     * @returns {Boolean}
     */
    static get formAssociated() {
      return supportsElementInternals;
    }

    constructor() {
      super();

      if (supportsElementInternals) {
        this._internals = this.attachInternals();
      }
    }

    connectedCallback() {
      super.connectedCallback();

      if (this.form) {
        this.form.addEventListener('reset', this.formResetCallback);
      }
    }

    disconnectedCallback() {
      if (this.form) {
        this.form.removeEventListener('reset', this.formResetCallback);
      }
      super.disconnectedCallback();
    }

    /**
     * Retrieve a reference to the associated form.
     * Returns null if not associated to any form.
     *
     * @return {HTMLFormElement}
     */
    get form() {
      if (this._internals) {
        return this._internals.form;
      }
      return null;
    }

    /**
     * Returns the validity state of the element.
     *
     * ```js
     * {
     *   badInput: false
     *   customError: false
     *   patternMismatch: false
     *   rangeOverflow: false
     *   rangeUnderflow: false
     *   stepMismatch: false
     *   tooLong: false
     *   tooShort: false
     *   typeMismatch: false
     *   valid: false
     *   valueMissing: true
     * }
     * ```
     *
     * @returns {ValidityState}
     */
    get validity() {
      if (this._internals) {
        return this._internals.validity;
      }
      return {};
    }

    /**
     * Set the validity of the control.
     *
     * @link https://developer.mozilla.org/en-US/docs/Web/API/ValidityState | ValidityState
     *
     * ```
     * // Marks internals's target element as suffering from the constraints
     * // indicated by the flags argument, and sets the element's validation message to message.
     * internals.setValidity(flags, message [, anchor ])
     *
     * // Marks internals's target element as satisfying its constraints.
     * internals.setValidity({})
     * ```
     *
     * @throws {DOMException} "NotSupportedError"
     * @param {ValidityStateFlags} flags - Validity flags
     * @param {DOMString} message - Optional message to supply
     * @param {HTMLElement} anchor - Optional element used by UA to display an interactive validation UI
     * @returns void
     */
    setValidity(flags, message, anchor) {
      if (this._internals) {
        this._internals.setValidity(flags, message, anchor);
      }
    }

    /**
     * Return the current validity of the element.
     *
     * Returns true if the element's value has no validity problems;
     * false otherwise. Fires an invalid event at the element in the latter case.
     *
     * @fires {Event} invalid
     * @return {Boolean}
     */
    checkValidity() {
      if (this._internals) {
        return this._internals.checkValidity();
      }
      return true;
    }

    /**
     * Return the current validity of the element.
     *
     * Returns true if the element's value has no validity problems;
     * otherwise, returns false, fires an invalid event at the element,
     * and (if the event isn't canceled) reports the problem to the user.
     *
     * @fires {Event} invalid
     * @returns {Boolean}
     */
    reportValidity() {
      if (this._internals) {
        return this._internals.reportValidity();
      }
      return true;
    }

    /**
     * Returns the error message that would be shown to the user
     * if internals's target element was to be checked for validity.
     *
     * @returns {DOMString}
     */
    get validationMessage() {
      if (this._internals) {
        return this._internals.validationMessage;
      }
      return '';
    }

    /**
     * Returns true if internals's target element will be validated
     * when the form is submitted; false otherwise.
     *
     * @returns {Boolean}
     */
    get willValidate() {
      if (this._internals) {
        return this._internals.willValidate;
      }
      return false;
    }

    /**
     * Associates the provided value (and optional state) on its parent form if one is present.
     * If the value is null, the element will not participate in the form submission process.
     *
     * @param {File | string | FormData | null} value - The value to set
     * @param {File | string | FormData | null} state - The state object provided to during session restores and when autofilling.
     */
    setFormValue(value, state) {
      if (this._internals) {
        this._internals.setFormValue(value, state || value);
      }
    }

    /**
     * When the form owner of a form-associated custom element is reset,
     * its formResetCallback is called.
     */
    formResetCallback() {
      this.value = this.getAttribute('value') || '';
    }

    /**
     * When the disabled state of a form-associated custom element is changed,
     * its formDisabledCallback is called, given the new state as an argument.
     */
    formDisabledCallback(disabled) {}
  };
