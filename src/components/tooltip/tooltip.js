import { LitElement, html } from 'lit';
import { createPopper } from '@popperjs/core';
import styles from './tooltip.styles.js';

export class Tooltip extends LitElement {
  static get properties() {
    return {
      placement: {
        type: String, // left, right, top, bottom
        reflect: true,
      },

      content: {
        type: String,
        reflect: true,
      },

      target: {
        type: String,
        reflect: true,
      },

      distance: {
        type: Number,
        reflect: true,
      },

      noArrow: {
        type: Boolean,
        attribute: 'no-arrow',
        reflect: true,
      },

      active: {
        type: Boolean,
        attribute: false,
      },
    };
  }

  constructor() {
    super();
    this.placement = 'top';
    this.active = false;
    this.instance = null;
    this.arrow = true;
    this.distance = 8;
    this._onMouseenter = this._onMouseenter.bind(this);
    this._onMouseleave = this._onMouseleave.bind(this);
  }

  get popperOptions() {
    return {
      placement: this.placement,
      modifiers: [
        {
          name: 'offset',
          options: {
            offset: [0, this.distance],
          },
        },
      ],
    };
  }

  _onMouseenter() {
    this.active = true;
    this.requestUpdate();
  }

  _onMouseleave() {
    this.active = false;
    this.requestUpdate();
  }

  _getTarget() {
    return (
      document.querySelector(this.target) ||
      (this.parentElement && this.parentElement.querySelector(this.target))
    );
  }

  setTooltip() {
    if (this.instance) return;

    const target = this._getTarget();
    const tooltip = this.shadowRoot.querySelector('#fw-tooltip');
    target.setAttribute('aria-describedby', 'fw-tooltip');
    if (!target) throw new Error('Tooltip target not defined');
    if (!tooltip) throw new Error('Tooltip not defined');
    this.instance = createPopper(target, tooltip, this.popperOptions);
  }

  updated() {
    super.updated();
    const target = this._getTarget();
    if (this.instance) this.instance.setOptions(this.popperOptions);
    if (!target) return;
    target.addEventListener('mouseover', this._onMouseenter);
    target.addEventListener('mouseleave', this._onMouseleave);
    this.setTooltip();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    const target = this._getTarget();
    if (this.instance) this.instance.destroy();
    if (!target) return;
    target.removeEventListener('mouseover', this._onMouseenter);
    target.removeEventListener('mouseleave', this._onMouseleave);
  }

  static get styles() {
    return styles;
  }

  render() {
    const tooltip = html`
      <div
        id="fw-tooltip"
        class="${this.active ? 'active' : ''}"
        role="tooltip"
      >
        ${this.noArrow
          ? null
          : html`<div class="fw-tooltip-arrow" data-popper-arrow></div>`}
        <div class="tooltip-content">
          <slot></slot>
        </div>
      </div>
    `;

    return tooltip;
  }
}

window.customElements.define('fw-tooltip', Tooltip);
