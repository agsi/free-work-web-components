import { html } from 'lit';
import './tooltip.js';

export default {
  title: 'Core/fw-tooltip',
  argTypes: {
    placement: {
      control: {
        type: 'radio',
        options: [
          'top',
          'bottom',
          'left',
          'right',
          'top-start',
          'top-end',
          'bottom-start',
          'bottom-end',
        ],
      },
    },

    distance: {
      control: { type: 'range', min: 0, max: 50, step: 1 },
      defaultValue: 8,
    },
  },
};

const Template = args => html`
  <div class="p-4 w-screen h-screen flex items-center justify-center">
    <button
      id="tooltip-target"
      class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center transition-all duration-100"
    >
      Hover me
    </button>
    <fw-tooltip
      target="#tooltip-target"
      content="${args.content}"
      placement="${args.placement}"
      distance="${args.distance}"
    >
      Hello world !
    </fw-tooltip>
  </div>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  placement: 'top',
  distance: 8,
  content: 'Hello world !',
};

export const Showcase = () => `
<div class="p-4 w-screen h-screen flex items-center justify-center">
  <div class="max-w-6xl mx-auto text-center grid grid-cols-4 gap-6">
    <div>
    <fw-tooltip target="#top" content="Hello world" placement="top" class="mr-2">Tooltip top</fw-tooltip>
      <button id="top" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip top
      </button>
    </div>
    <div>
    <fw-tooltip target="#top-start" content="Hello world" placement="top-start" class="mr-2">Tooltip top-start</fw-tooltip>
      <button id="top-start" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip top-start
      </button>
    </div>
    <div>
    <fw-tooltip target="#top-end" content="Hello world" placement="top-end" class="mr-2">Tooltip top-end</fw-tooltip>
      <button id="top-end" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip top-end
      </button>
    </div>
    <div>
    <fw-tooltip target="#bottom" content="Hello world" placement="bottom" class="mr-2">Tooltip bottom</fw-tooltip>
      <button id="bottom" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip bottom
      </button>
    </div>
    <div>
    <fw-tooltip target="#bottom-start" content="Hello world" placement="bottom-start" class="mr-2">Tooltip bottom-start</fw-tooltip>
      <button id="bottom-start" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip bottom-start
      </button>
    </div>
    <div>
    <fw-tooltip target="#bottom-end" content="Hello world" placement="bottom-end" class="mr-2">Tooltip bottom-end</fw-tooltip>
      <button id="bottom-end" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip bottom-end
      </button>
    </div>
    <div>
    <fw-tooltip target="#left" content="Hello world" placement="left" class="mr-2">Tooltip left</fw-tooltip>
      <button id="left" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip left
      </button>
    </div>
    <div>
    <fw-tooltip target="#left-start" content="Hello world" placement="left-start" class="mr-2">Tooltip left-start</fw-tooltip>
      <button id="left-start" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip left-start
      </button>
    </div>
    <div>
    <fw-tooltip target="#left-end" content="Hello world" placement="left-end" class="mr-2">Tooltip left-end</fw-tooltip>
      <button id="left-end" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip left-end
      </button>
    </div>
    <div>
    <fw-tooltip target="#right" content="Hello world" placement="right" class="mr-2">Tooltip right</fw-tooltip>
      <button id="right" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip right
      </button>
    </div>
    <div>
    <fw-tooltip target="#right-start" content="Hello world" placement="right-start" class="mr-2">Tooltip right-start</fw-tooltip>
      <button id="right-start" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip right-start
      </button>
    </div>
    <div>
    <fw-tooltip target="#right-end" content="Hello world" placement="right-end" class="mr-2">Tooltip right-end</fw-tooltip>
      <button id="right-end" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center justify-center transition-all duration-100">
        Tooltip right-end
      </button>
    </div>
  </div>
</div>
`;
