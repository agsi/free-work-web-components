import { css } from 'lit';

export default css`
  :host {
    --tooltip-border-radius: 4px;
    --tooltip-background-color: #ffffff;
    --tooltip-text-color: inherit;
    --tooltip-border-color: transparent;
    --tooltip-padding: 4px 8px;
    --tooltip-max-width: 180px;
    --tooltip-z-index: 10;
    --tooltip-shadow: 0 1px 2px rgba(0, 0, 0, 0.16);
    position: fixed;
    pointer-events: none;
  }

  :host * {
    box-sizing: border-box;
  }

  #fw-tooltip {
    display: block !important;
    color: var(--tooltip-text-color);
    background-color: var(--tooltip-background-color);
    font-size: 13px;
    border-radius: var(--tooltip-border-radius);
    filter: drop-shadow(var(--tooltip-shadow));
    z-index: var(--tooltip-z-index);
    max-width: var(--tooltip-max-width);
    width: max-content;
    transition: opacity 0.1s ease-out;
    pointer-events: auto;
  }

  .tooltip-content {
    padding: var(--tooltip-padding);
  }

  #fw-tooltip:not(.active) {
    visibility: hidden;
    opacity: 0;
    pointer-events: none;
    transition: all 0.1s ease-in;
  }

  .fw-tooltip-arrow {
    position: absolute;
  }

  .fw-tooltip-arrow::before {
    position: absolute;
    width: 8px;
    height: 8px;
    top: -4px;
    left: -4px;
  }

  #fw-tooltip-arrow {
    visibility: hidden;
  }

  .fw-tooltip-arrow::before {
    visibility: visible;
    content: '';
    transform: rotate(45deg);
    background-color: var(--tooltip-background-color);
  }

  #fw-tooltip[data-popper-placement^='top'] .fw-tooltip-arrow {
    bottom: 0px;
  }

  #fw-tooltip[data-popper-placement^='bottom'] .fw-tooltip-arrow {
    top: 0px;
  }

  #fw-tooltip[data-popper-placement^='left'] .fw-tooltip-arrow {
    right: uset;
    left: 100%;
  }

  #fw-tooltip[data-popper-placement^='right'] .fw-tooltip-arrow {
    right: 100%;
    left: unset;
  }
`;
