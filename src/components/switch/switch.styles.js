import { css } from 'lit';

export default css`
  :host {
    /* Main color hue (HSL) */
    --color-hue: 201;
    --height: 1rem;
    --width: calc(var(--height) * 2);
    --thumb-size: calc(var(--height) + 4px);
    --thumb-background: white;
    --thumb-border: 1px solid #e5e7eb;
    --thumb-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1),
      0 1px 2px 0 rgba(0, 0, 0, 0.06);
    --track-runnable-color: #e5e7eb;
    --items-alignment: center;

    display: inline-block;
  }

  .switch {
    --track-color: hsl(var(--color-hue), 100%, 50%);
    --track-color-hover: hsl(var(--color-hue), 100%, 60%);

    display: inline-flex;
    align-items: var(--items-alignment);
    gap: 0.5em;
    line-height: var(--height);
    cursor: pointer;
    user-select: none;
  }

  /* Disabled */
  .switch--disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }

  .switch__control {
    position: relative;
    display: inline-flex;
    flex: 0 0 auto;
    align-items: center;
    justify-content: center;
    width: var(--width);
    height: var(--height);
    background-color: var(--track-runnable-color);
    border: 1px solid var(--track-runnable-color);
    border-radius: var(--height);
    transition: 150ms border-color, 150ms background-color;
  }

  .switch__control .switch__thumb {
    width: var(--thumb-size);
    height: var(--thumb-size);
    background: var(--thumb-background);
    border: var(--thumb-border);
    border-radius: 50%;
    box-shadow: var(--thumb-shadow);
    transform: translateX(calc((var(--width) - var(--height)) / -2));
    transition: 150ms transform ease, 150ms background-color, 150ms border-color,
      150ms box-shadow;
  }

  .switch__input {
    position: absolute;
    opacity: 0;
    padding: 0;
    margin: 0;
    pointer-events: none;
  }

  /* Checked */
  .switch--checked .switch__control {
    background-color: var(--track-color);
    border-color: var(--track-color);
  }

  .switch--checked .switch__control .switch__thumb {
    background-color: var(--thumb-background);
    border-color: var(--track-color);
    transform: translateX(calc((var(--width) - var(--height)) / 2));
  }

  /* Focus */
  /*
  .switch:not(.switch--checked):not(.switch--disabled)
    .switch__input:focus-visible
    ~ .switch__control {
    background-color: var(--track-runnable-color);
    border: 1px solid var(--track-color);
  }

  .switch:not(.switch--checked):not(.switch--disabled)
    .switch__input:focus-visible
    ~ .switch__control
    .switch__thumb {
    border-color: var(--track-color);
  }

  .switch.switch--checked:not(.switch--disabled)
    .switch__input:focus-visible
    ~ .switch__control {
    background-color: var(--track-runnable-color);
    border: 1px solid var(--track-color);
  }

  .switch.switch--checked:not(.switch--disabled)
    .switch__input:focus-visible
    ~ .switch__control
    .switch__thumb {
    box-shadow: 0 0 0 3px hsl(var(--color-hue), 95%, 80%);
  }
  */
`;
