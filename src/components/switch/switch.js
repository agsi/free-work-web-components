import { html, LitElement } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { live } from 'lit/directives/live.js';
import { FormAssociatedMixin } from '../../mixins/form-associated-mixin.js';
import { dispatchEvent } from '../../internals/event.js';
import styles from './switch.styles.js';

/**
 * A Switch Custom HTML Element.
 *
 * Represents on/off values, as opposed to checked/unchecked values.
 *
 * @link https://www.w3.org/TR/wai-aria-1.1/#switch | ARIA switch.
 */
export class Switch extends FormAssociatedMixin(LitElement) {
  static get styles() {
    return styles;
  }

  static get properties() {
    return {
      name: {
        type: String,
        reflect: true,
      },

      value: {
        type: Number,
      },

      checked: {
        type: Boolean,
        reflect: true,
      },

      disabled: {
        type: Boolean,
        reflect: true,
      },

      invalid: {
        type: Boolean,
        reflect: true,
      },

      required: {
        type: Boolean,
        reflect: true,
      },
    };
  }

  constructor() {
    // Always call super() first
    super();

    const id = Math.random().toString(36).substring(2, 8);
    this.switchId = `switch-${id}`;
    this.labelId = `switch-label-${id}`;

    // Initialize properties
    this.defaultChecked = !!this.getAttribute('checked');
    this.checked = this.defaultChecked;
    // Default to "on" to reach parity with input[type="checkbox"]
    this.value = 'on';
  }

  get input() {
    return this.shadowRoot.querySelector('input[type="checkbox"]');
  }

  click() {
    this.input.click();
  }

  focus(options) {
    this.input.focus(options);
  }

  blur() {
    this.input.blur();
  }

  handleBlur() {
    this.hasFocus = false;
  }

  handleFocus() {
    this.hasFocus = true;
  }

  handleInputChange({ currentTarget }) {
    this.checked = currentTarget.checked;
  }

  handleMouseDown(event) {
    event.preventDefault();
    this.input.focus();
  }

  validate() {
    const valueMissing =
      this.required && !this.disabled && this.checked === this.defaultChecked;

    if (valueMissing) {
      this.setValidity(
        this.input.validity,
        this.input.validationMessage,
        this.input
      );
    } else {
      this.setValidity({});
    }
  }

  reportValidity() {
    return this.input.reportValidity();
  }

  setCustomValidity(message) {
    this.input.setCustomValidity(message);
    this.validate();
  }

  formDisabledCallback(disabled) {
    this.disabled = disabled;
  }

  formResetCallback() {
    this.checked = this.defaultChecked;
  }

  updated(changedProperties) {
    if (changedProperties.has('checked')) {
      const value = this.checked ? this.value : null;
      this.validate();
      this.setFormValue(value, value);

      if (changedProperties.get('checked') !== undefined) {
        dispatchEvent(this, 'change', { value });
      }
    }

    if (
      changedProperties.has('required') ||
      changedProperties.has('invalid') ||
      changedProperties.has('disabled')
    ) {
      this.validate();
    }
  }

  render() {
    return html`
      <label
        for=${this.switchId}
        class=${classMap({
          switch: true,
          'switch--checked': this.checked,
          'switch--disabled': this.disabled,
          'switch--focused': this.hasFocus,
        })}
        @mousedown=${this.handleMouseDown}
      >
        <input
          id=${this.switchId}
          type="checkbox"
          role="switch"
          aria-checked=${this.checked ? 'true' : 'false'}
          aria-labelledby=${this.labelId}
          class="switch__input"
          name=${ifDefined(this.name)}
          value=${ifDefined(this.value)}
          .checked=${live(this.checked)}
          .disabled=${this.disabled}
          .required=${this.required}
          @change=${this.handleInputChange}
          @blur=${this.handleBlur}
          @focus=${this.handleFocus}
        />
        <span part="control" class="switch__control">
          <span part="thumb" class="switch__thumb"></span>
        </span>
        <span part="label" id=${this.labelId} class="switch__label">
          <slot></slot>
        </span>
      </label>
    `;
  }
}

window.customElements.define('fw-switch', Switch);
