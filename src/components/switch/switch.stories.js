import { html } from 'lit';
import './switch.js';

export default {
  title: 'Core/fw-switch',
  argTypes: {
    onChange: { action: 'change' },
    width: {
      control: { type: 'range', min: 10, max: 200, step: 1 },
    },
    thumbSize: {
      control: { type: 'range', min: 10, max: 200, step: 1 },
    },
  },
};

const Template = args => html`
  <form onsubmit="return false">
    <fieldset class="flex items-center gap-8">
      <fw-switch
        id="accept_tos"
        name="${args.name}"
        .required="${args.required}"
        .disabled="${args.disabled}"
        .invalid="${args.invalid}"
        .checked="${args.checked}"
        @change="${args.onChange}"
      >
        Default size
      </fw-switch>

      <fw-switch
        style="--width: ${args.width}px; --height: 28px; --thumb-size: ${args.thumbSize}px;"
        name="${args.name}_"
        .disabled="${args.disabled}"
        .invalid="${args.invalid}"
        .checked="${args.checked}"
        @change="${args.onChange}"
      >
        Custom size
      </fw-switch>
    </fieldset>

    <div class="flex items-center gap-4 py-8">
      <button class="border rounded px-6 py-1" type="reset">Reset</button>
      <button class="border rounded px-6 py-1" type="submit">Save</button>
    </div>
  </form>

  <script>
    document.addEventListener('DOMContentLoaded', () => {
      const switchElement = document.querySelector('#accept_tos');
      if (!switchElement.input) return; // skip SB simulateDOMContentLoaded
      switchElement.setCustomValidity('${args.validationMessage}');
    });
  </script>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  name: 'agree_tos',
  checked: false,
  disabled: false,
  required: false,
  invalid: false,
  validationMessage: '',
  width: 60,
  thumbSize: 22,
};
