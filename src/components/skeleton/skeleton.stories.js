import { html } from 'lit';
import './skeleton.js';

export default {
  title: 'Core/fw-skeleton',
  argTypes: {
    animation: {
      control: {
        type: 'inline-radio',
        options: ['pulse', 'wave'],
      },
      defaultValue: 'pulse',
    },
    shape: {
      control: {
        type: 'inline-radio',
        options: ['circle', 'rectangle', 'text'],
      },
      defaultValue: 'text',
    },
  },
};

const Template = args => html`
  <fw-skeleton
    animation="${args.animation}"
    shape="${args.shape}"
    width="300"
    height="${args.shape === 'circle' ? '300' : '30'}"
  ></fw-skeleton>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {};

export const Showcase = () => `
  <div class="grid grid-cols-2 gap-4 p-4">${['pulse', 'wave']
    .map(
      animation => `
      <div>
        <div class="mb-2">
          <fw-skeleton animation="${animation}" shape="circle" width="50" height="50"></fw-skeleton>
        </div>
        <div class="mb-2">
          <fw-skeleton animation="${animation}"  shape="text" width="220" height="20"></fw-skeleton>
          <fw-skeleton animation="${animation}"  shape="text" width="220" height="20"></fw-skeleton>
          </div>
        <div class="mb-2">
          <fw-skeleton animation="${animation}"  shape="rectangle" width="220" height="80"></fw-skeleton>
        </div>
      </div>
    `
    )
    .join('\n')}</div>
`;
