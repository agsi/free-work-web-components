import { html } from 'lit';
import './button.js';

export default {
  title: 'Core/fw-button',
  argTypes: {
    type: {
      control: {
        type: 'inline-radio',
        options: ['button', 'submit'],
      },
      defaultValue: 'button',
    },
    size: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large'],
      },
      defaultValue: 'small',
    },
    variant: {
      control: {
        type: 'select',
        options: ['primary', 'secondary', 'outline-primary'],
      },
      defaultValue: 'primary',
    },
    onClick: { action: 'click' },
  },
};

const Template = args => html`
  <fw-button
    @click="${args.onClick}"
    type="${args.type}"
    name="${args.name}"
    href="${args.href}"
    size="${args.size}"
    variant="${args.variant}"
    .disabled="${args.disabled}"
    .loading="${args.loading}"
    .block="${args.block}"
    .fake="${args.fake}"
  >
    Sign in
  </fw-button>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  name: '',
  href: '',
  type: 'button',
  size: 'small',
  variant: 'primary',
  disabled: false,
  loading: false,
  block: false,
  fake: false,
};

/*
export const Large = Template.bind({});
Large.args = {
  size: 'large',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
};
*/

export const Showcase = () => html`
  <h2 class="mb-2">Primary</h2>
  <div class="mb-4">
    <fw-button variant="primary" size="large">Previous</fw-button>
    <fw-button variant="primary" size="medium">Previous</fw-button>
    <fw-button variant="primary" size="small">Previous</fw-button>
  </div>

  <h2 class="mb-2">Outline Primary</h2>
  <div class="mb-4">
    <fw-button variant="outline-primary" size="large">Previous</fw-button>
    <fw-button variant="outline-primary" size="medium">Previous</fw-button>
    <fw-button variant="outline-primary" size="small">Previous</fw-button>
  </div>

  <h2 class="mb-2">Secondary</h2>
  <div class="mb-4">
    <fw-button variant="secondary" size="large">Previous</fw-button>
    <fw-button variant="secondary" size="medium">Previous</fw-button>
    <fw-button variant="secondary" size="small">Previous</fw-button>
  </div>

  <h2 class="mb-2">Link</h2>
  <div class="mb-4">
    <fw-button href="https://pika.dev">Visit Pika.dev</fw-button>
  </div>
`;
