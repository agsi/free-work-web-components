import { html } from 'lit';
import './spinner.js';

export default {
  title: 'Core/fw-spinner',
  argTypes: {
    type: {
      control: {
        type: 'inline-radio',
        options: ['beat', 'square', 'circle'],
      },
      defaultValue: 'beat',
    },
    size: {
      control: { type: 'range', min: 1, max: 64, step: 1 },
    },
  },
};

const Template = args => html`
  <fw-spinner
    class="text-xxx"
    type="${args.type}"
    size="${args.size}"
  ></fw-spinner>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  size: 6,
};

export const Showcase = () => `
  <div>
    <fw-spinner class="block mb-8" type="square" size="8"></fw-spinner>
    <fw-spinner class="block mb-8" type="beat" size="8"></fw-spinner>
    <fw-spinner class="block mb-8" type="circle" size="20"></fw-spinner>
  </div>
`;
