import { css } from 'lit';

export default css`
  :host {
    --toast-border-radius: 6px;
    --toast-background-color: #fff;
    --toast-padding: 20px 20px 20px 20px;
    --toast-max-width: 300px;
    --toast-margin: 0 16px 16px 16px;
    --toast-z-index: 9010;
    --toast-color: inherit;
    --toast-border: none;
    --toast-shadow: 0 2px 4px rgba(0, 0, 0, 0.08),
      0 4px 16px rgba(0, 0, 0, 0.08);

    position: fixed;
    inset: 0;
    margin: var(--toast-margin);
    display: flex;
    pointer-events: none;
    z-index: var(--toast-z-index);
    transition: margin 0.3s;
  }

  .toast {
    padding: var(--toast-padding);
    width: 100%;
    height: auto;
    max-width: var(--toast-max-width);
    background-color: var(--toast-background-color);
    border-radius: var(--toast-border-radius);
    overflow: hidden;
    line-height: 1.5;
    box-shadow: var(--toast-shadow);
    pointer-events: none;
    position: relative;
    opacity: 0;
    visibility: hidden;
    color: var(--toast-color);
    border: var(--toast-border);
  }

  .toast.bottom-center {
    align-self: flex-end;
    margin-left: auto;
    margin-right: auto;
  }

  .toast.bottom-left {
    align-self: flex-end;
    margin-left: 0;
    margin-right: auto;
  }

  .toast.bottom-right {
    align-self: flex-end;
    margin-left: auto;
    margin-right: 0;
  }

  .toast.active {
    animation: toast-fade-in 0.3s cubic-bezier(0.23, 1, 0.32, 1);
    animation-fill-mode: forwards;
    pointer-events: initial;
  }

  .toast.closing {
    animation: toast-fade-out 0.3s cubic-bezier(0.23, 1, 0.32, 1);
    animation-fill-mode: forwards;
    pointer-events: initial;
  }

  @keyframes toast-fade-in {
    from {
      transform: translateY(1rem) translateZ(0) scale(0.9);
      opacity: 0;
      visibility: hidden;
    }
    to {
      transform: none;
      opacity: 1;
      visibility: visible;
    }
  }

  @keyframes toast-fade-out {
    from {
      transform: none;
      opacity: 1;
      visibility: visible;
    }
    to {
      transform: translateY(1rem) translateZ(0) scale(0.9);
      opacity: 0;
      visibility: hidden;
    }
  }

  .close-button {
    height: 24px;
    width: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    background: none;
    border: none;
    padding: 0;
    position: absolute;
    top: 8px;
    right: 8px;
    opacity: 0.7;
    transition: opacity 0.3s;
    cursor: pointer;
  }

  .close-button:hover,
  .close-button:focus {
    opacity: 1;
  }

  .close-button svg {
    height: 20px;
    width: 20px;
  }

  .close-button svg path {
    fill: var(--toast-color);
  }
`;
