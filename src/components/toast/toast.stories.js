import { html } from 'lit';
import './toast.js';

const openToast = () => {
  const toast = document.querySelector('#toast-el');
  toast.open();
};

const closeToast = () => {
  const toast = document.querySelector('#toast-el');
  toast.close();
  toast.onclose = () => alert('Toast closed');
};

export default {
  title: 'Core/fw-toast',
  argTypes: {
    closeable: {
      control: {
        type: 'boolean',
      },
    },
    position: {
      control: {
        type: 'select',
        options: ['bottom-center', 'bottom-left', 'bottom-right'],
      },
    },
  },
};

const Template = args => html`
  <div class="p-4 w-screen h-screen flex items-center justify-center">
    <button
      class="bg-gray-200 px-4 py-3 inline-fle font-semibold rounded-sm mr-2"
      @click="${openToast}"
    >
      Open toast
    </button>
    <button
      class="bg-gray-200 px-4 py-3 inline-fle font-semibold rounded-sm mr-2"
      @click="${closeToast}"
    >
      Close toast
    </button>
    <fw-toast
      id="toast-el"
      position="${args.position}"
      ?closeable="${args.closeable}"
    >
      Idioms are a wonderful part of the English language that gives it
      flavor.</fw-toast
    >
  </div>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  position: 'bottom-center',
  closeable: true,
};

export const Showcase = () => `
  <div class="grid grid-cols-3 gap-4">
    <div class="col-span-1">
      <fw-toast
      active
      class="showcased-toasts"
    >This toast can handle a simple message</fw-toast>
    </div>
    <div class="col-span-1">
      <fw-toast
      active
      class="showcased-toasts"
      closeable
    >It can be closeable.</fw-toast>
    </div>
    <div class="col-span-1">
      <fw-toast
        active
        closeable
        class="showcased-toasts"
      >
        <h3 class="text-xl font-semibold mb-3">
          How about custom layouts ?
        </h3>
        <p class="mb-4" >Put whatever you want in here, make it pop. It's all good.</p>
        <div class="flex">
          <button class="ml-auto px-4 py-2 bg-blue-500 text-white rounded-sm">
            Okay
          </button>
        </div>
      </fw-toast>
    </div>
    <div class="col-span-1">
      <fw-toast
        active
        closeable
        class="showcased-toasts"
      >
        <h3 class="text-xl font-semibold mb-3">
          Like, whatever, really.
        </h3>
        <iframe width="100%" src="https://www.youtube.com/embed/SYCWtytwWI0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </fw-toast>
    </div>
    <div class="col-span-1">
      <fw-toast
        active
        closeable
        class="showcased-toasts"
      >
        <h3 class="text-xl font-semibold mb-3">
          Never miss an opportunity again.
        </h3>
        <p class="mb-6" >Give us your email and we will get back to you with some great stuffs.</p>

        <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
          Email
        </label>
        <div class="flex">
          <input class="shadow appearance-none border rounded-l-sm w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="email" type="email" placeholder="example@domain.com">
          <button class="ml-auto bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-r-sm focus:outline-none focus:shadow-outline" type="button">
            Go
          </button>
        </div>
      </fw-toast>
    </div>
  </div>

  <style>
    .showcased-toasts {
      position: unset;
      width: 100%;
      margin: 0;
    }
  </style>
`;
