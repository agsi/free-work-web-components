import { LitElement, html } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import styles from './toast.styles.js';

export class Toast extends LitElement {
  static get properties() {
    return {
      active: {
        type: Boolean,
        reflect: true,
      },

      position: {
        type: String, // 'bottom-left', 'bottom-right', 'bottom-center'
        reflect: true,
      },

      closeable: {
        type: Boolean,
        reflect: true,
      },
    };
  }

  constructor() {
    super();
    this.position = 'bottom-center';
    this.active = false;
    this.closeEvent = new Event('close');
    this.closing = false;
    this._onKeyDown = this._onKeyDown.bind(this);
  }

  static get styles() {
    return styles;
  }

  firstUpdated() {
    super.firstUpdated();
  }

  open() {
    if (this.active) return;
    this.active = true;
    document.addEventListener('keydown', this._onKeyDown);
  }

  close() {
    if (!this.active) return;
    this.closing = true;
    this.active = false;
    document.removeEventListener('keydown', this._onKeyDown);
  }

  _onAnimationEnd() {
    if (this.closing) {
      this.closing = false;
      this.dispatchEvent(this.closeEvent);
      this.requestUpdate();
    }
  }

  _onKeyDown({ key }) {
    if (key === 'Escape') {
      this.close();
    }
  }

  render() {
    const classes = {
      toast: true,
      active: this.active,
      [this.position]: this.position,
      closing: this.closing,
    };

    const closeButton = html`
      <button @click="${this.close}" class="close-button">
        <svg
          aria-hidden="true"
          focusable="false"
          data-prefix="fas"
          data-icon="xmark"
          role="img"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 320 512"
        >
          <path
            fill="#002c47"
            d="M310.6 361.4c12.5 12.5 12.5 32.75 0 45.25C304.4 412.9 296.2 416 288 416s-16.38-3.125-22.62-9.375L160 301.3L54.63 406.6C48.38 412.9 40.19 416 32 416S15.63 412.9 9.375 406.6c-12.5-12.5-12.5-32.75 0-45.25l105.4-105.4L9.375 150.6c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 210.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-105.4 105.4L310.6 361.4z"
          ></path>
        </svg>
      </button>
    `;

    const toast = html` <div
      class="${classMap(classes)}"
      @animationend="${this._onAnimationEnd}"
    >
      ${this.closeable ? closeButton : null}
      <div class="toast-body">
        <slot></slot>
      </div>
    </div>`;

    return this.closing || this.active ? toast : null;
  }
}

window.customElements.define('fw-toast', Toast);
