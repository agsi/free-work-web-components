import { html, LitElement } from 'lit';
import { dispatchEvent } from '../../internals/event.js';
import styles from './file-upload.styles.js';

/**
 * List of events:
 * @fires {CustomEvent} `file-add` - Fires when a local file is chosen or dropped.
 * @fires {CustomEvent} `file-reject` - Fires when a file is rejected by its validation contraints.
 * @fires {CustomEvent} `upload-error` - Fires when the upload request failed.
 * @fires {CustomEvent} `upload-success` - Fires when the upload request succeeded.
 */
export class FileUpload extends LitElement {
  static get styles() {
    return styles;
  }

  static get properties() {
    return {
      /**
       * Form name used by Content-Disposition.
       *
       * When dealing with multiple files in the same field, there can be several
       * subparts with the same name.
       */
      name: {
        type: String,
      },

      /**
       * Defines the file types the file input should accept.
       *
       * This string is a comma-separated list of MIME type patterns or file extensions.
       * e.g. `accept=".doc,.docx,application/msword"`
       *
       * @link https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input/file#accept
       * @link https://caniuse.com/input-file-accept
       */
      accept: {
        type: String,
      },

      /**
       * Array of File objects beeing uploaded.
       *
       * @link https://developer.mozilla.org/en-US/docs/Web/API/File
       */
      files: {
        type: Array,
      },

      /**
       * Timeout in milliseconds for the upload request (0 for no timeout).
       */
      timeout: {
        type: Number,
      },

      /**
       * Maximum number of files the user can choose.
       */
      maxFiles: {
        type: Number,
        reflect: true,
      },

      /**
       * HTTP method used by the upload request.
       */
      method: {
        type: String,
      },

      /**
       * Maximum size per file. Size is in MO.
       */
      maxFileSize: {
        type: Number,
      },

      /**
       * Upload server URL.
       */
      url: {
        type: String,
      },

      /**
       * Prevents immediate upload to the server.
       * When disabled, the `uploadFiles` method must be called manually.
       */
      noAuto: {
        type: Boolean,
      },
    };
  }

  constructor() {
    // Always call super() first
    super();

    // Initialize properties
    this.accept = '';
    this.name = '';
    this.files = [];
    this.timeout = 0;
    this.maxFiles = 1;
    // this.maxFilesSize = Infinity;
    this.maxFileSize = 10; // In MO
    this.method = 'POST';
    this.url = '';
    this.noAuto = false;

    // Initialize events listeners
    this._onDragover = this._onDragover.bind(this);
    this._onDragleave = this._onDragleave.bind(this);
    this._onDrop = this._onDrop.bind(this);
  }

  connectedCallback() {
    super.connectedCallback();
    // https://caniuse.com/dragndrop
    this.addEventListener('dragover', this._onDragover);
    this.addEventListener('dragleave', this._onDragleave);
    this.addEventListener('drop', this._onDrop);
  }

  disconnectedCallback() {
    this.removeEventListener('dragover', this._onDragover);
    this.removeEventListener('dragleave', this._onDragleave);
    this.removeEventListener('drop', this._onDrop);
    super.disconnectedCallback();
  }

  get fileInput() {
    return this.shadowRoot.querySelector('[type="file"]');
  }

  render() {
    return html`<div>
      <div @click="${this._onAddFilesClick}">
        <div class="upload-buttons">
          <slot name="upload-buttons"></slot>
        </div>

        <input
          hidden
          type="file"
          @change="${this._onFileInputChange}"
          .multiple="${this.maxFiles > 1}"
          .accept="${this.accept}"
        />
        <slot name="file-list">
          ${this.files.length > 0
            ? html`
                <ul class="file-list">
                  ${this.files.map(
                    file =>
                      html`<li>
                        <div part="file-name" id="name">${file.name}</div>
                      </li>`
                  )}
                </ul>
              `
            : ''}
        </slot>
      </div>
    </div>`;
  }

  reset() {
    this.fileInput.value = '';
    this.files = [];
  }

  /**
   * Upload staging files.
   */
  uploadFiles() {
    const files = this.files.filter(file => !file.complete);
    Array.prototype.forEach.call(files, this._uploadFile.bind(this));
  }

  maxFilesReached() {
    return this.files.length > this.maxFiles;
  }

  _onFileInputChange(event) {
    this._addFiles(event.target.files);
  }

  _addFiles(files) {
    Array.prototype.forEach.call(files, this._addFile.bind(this));
  }

  _addFile(file) {
    if (this.maxFilesReached()) {
      dispatchEvent(this, 'file-reject', { file, maxFilesReached: true });
      return;
    }

    const fileExt = file.name.match(/\.[^.]*$|$/)[0];
    const re = new RegExp(
      '^(' + this.accept.replace(/[, ]+/g, '|').replace(/\/\*/g, '/.*') + ')$',
      'i'
    );
    if (this.accept && !(re.test(file.type) || re.test(fileExt))) {
      dispatchEvent(this, 'file-reject', { file, fileTypeError: true });
      return;
    }

    // Convert file size in MO
    const size = file.size / Math.pow(1024, 2);
    if (size >= this.maxFileSize) {
      dispatchEvent(this, 'file-reject', { file, maxSizeReached: true });
      return;
    }

    file.loaded = 0;
    this.files.unshift(file);

    dispatchEvent(this, 'file-add', { file });

    if (!this.noAuto) {
      this._uploadFile(file);
    }

    this.requestUpdate();
  }

  _removeFile(file) {
    if (this.files.includes(file)) {
      this.splice('files', this.files.indexOf(file), 1);
    }
  }

  // https://developer.mozilla.org/fr/docs/Web/API/File
  _uploadFile(file) {
    if (file.uploading) {
      return;
    }

    // const startTime = Date.now();
    const formData = new FormData();
    file.url = file.url || this.url || '';
    formData.append(this.name, file, file.name);

    const xhr = (file.xhr = new XMLHttpRequest());
    xhr.upload.onprogress = e => {
      this._onUploadProgress(e, file, xhr);
    };

    xhr.timeout = this.timeout;
    xhr.onreadystatechange = e => {
      if (xhr.readyState === 4) {
        if (xhr.status === 0 || xhr.status >= 400) {
          file.error = xhr.status;
        }
        file.uploading = file.indeterminate = false;
        file.complete = !file.error;

        dispatchEvent(this, `upload-${file.error ? 'error' : 'success'}`, {
          file,
          xhr,
        });
      }
    };

    // Custom event to let modify the xhr before executing the upload
    // E.g. `event.detail.formData.append('otherField', 'value')`
    dispatchEvent(this, 'upload-request', { file, xhr, formData });

    // xhr.withCredentials = this.withCredentials;
    xhr.open(this.method, file.url, true);

    file.uploading = file.indeterminate = true;
    file.complete = file.error = false;

    xhr.send(formData);
  }

  _onUploadProgress(event, file, xhr) {
    const loaded = event.loaded;
    const total = event.total;
    const progress = ~~((loaded / total) * 100);
    file.loaded = loaded;
    file.progress = progress;
    file.indeterminate = loaded <= 0 || loaded >= total;

    dispatchEvent(this, 'upload-progress', { file, xhr });
  }

  _onAddFilesClick() {
    if (this.maxFilesReached()) {
      return;
    }

    this.fileInput.value = '';
    this.fileInput.click();
  }

  _onDragover(event) {
    event.preventDefault();

    const maxFilesReached = this.maxFilesReached();
    this.setAttribute('dragover', 'dragover');
    // https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer/dropEffect
    event.dataTransfer.dropEffect = maxFilesReached ? 'none' : 'copy';
  }

  _onDragleave(event) {
    this.removeAttribute('dragover');
  }

  _onDrop(event) {
    event.preventDefault();
    this.removeAttribute('dragover');
    this._addFiles(event.dataTransfer.files);
  }
}

window.customElements.define('fw-file-upload', FileUpload);
