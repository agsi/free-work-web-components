import { html } from 'lit';
import './file-upload.js';

export default {
  title: 'Core/fw-file-upload',
};

const Template = args => html`
  <div class="flex flex-col gap-8 m-4 max-w-3xl">
    <fw-file-upload
      name="${args.name}"
      maxFiles="${args.maxFiles}"
      url="${args.url}"
    >
    </fw-file-upload>

    <fw-file-upload
      class="flex justify-center border-2 border-gray-300 border-dashed rounded-md py-8"
      name="${args.name}"
      maxFiles="${args.maxFiles}"
      url="${args.url}"
    >
      <button
        slot="upload-button"
        class="border-0 bg-blue-900 text-white px-4 py-2 text-base rounded-full"
      >
        Chargez votre CV
      </button>
      <div slot="drop-label">Déposez votre CV ici</div>
    </fw-file-upload>
  </div>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  accept: '.doc,.pdf',
  name: 'resume',
  maxFiles: 1,
  url: 'http://httpbin.org/post',
};
