import { css } from 'lit';

export default css`
  :host {
    --upload-area-border: 2px dashed #e5e7eb;
    --upload-button-background-color: #e5e7eb;
    --upload-button-text-color: #1676f3;

    display: inline-block;
    overflow: hidden;
    transition: background-color 200ms, border-color 200ms;
    border: var(--upload-area-border);
    border-radius: 2px;
    padding: 16px;
    background-color: white;
  }

  :host([dragover]) {
    border-color: #1676f31a;
    background: #1676f31a;
  }

  :host([hidden]) {
    display: none !important;
  }

  [hidden] {
    display: none !important;
  }

  .file-list {
    padding: 0;
    margin: 0;
    list-style: none;
    margin-top: 16px;
  }

  .upload-buttons {
    display: flex;
    align-items: center;
    gap: 16px;
  }

  .upload-button {
    appearance: none;
    background: var(--upload-button-background-color);
    color: var(--upload-button-text-color);
    border: 0;
    padding: 8px 16px;
    font-weight: 500;
    font-size: 1rem;
  }

  [part='drop-label'] {
    display: flex;
    align-items: center;
    gap: 8px;
  }
`;
