import { html } from 'lit';
import './marquee.js';

export default {
  title: 'Core/fw-marquee',
  argTypes: {
    direction: {
      control: {
        type: 'select',
        options: ['left', 'right'],
      },
      defaultValue: 'left',
    },

    speed: {
      control: {
        type: 'range',
        min: 100,
        max: 500,
      },
      defaultValue: 150,
    },

    fade: {
      control: 'boolean',
      defaultValue: true,
    },

    hidden: {
      control: 'boolean',
      defaultValue: false,
    },
  },
};

const Template = args => html`
  <fw-marquee
    direction="${args.direction}"
    speed="${args.speed}"
    ?hidden="${args.hidden}"
    ?fade="${args.fade}"
  >
    <p class="p-2">
      Suspendisse leo dolor, ullamcorper a condimentum vel, mattis eget nibh.
      Etiam eleifend tellus nibh, sit amet sagittis felis sodales quis. Maecenas
      ut massa eget quam gravida ultrices. Sed eu mi ullamcorper, lobortis
      libero eu, varius tortor. Nam sit amet justo ut magna blandit placerat ut
      sit amet odio. Integer eget viverra magna. Quisque vehicula arcu vel
      lectus lacinia tincidunt. Aenean efficitur urna et sem iaculis gravida
    </p>
  </fw-marquee>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  direction: 'left',
  speed: 10,
};
