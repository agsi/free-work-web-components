import { html, LitElement } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { styleMap } from 'lit/directives/style-map.js';
import styles from './marquee.styles.js';

export class Marquee extends LitElement {
  static get styles() {
    return styles;
  }

  static get properties() {
    return {
      direction: {
        type: String,
        reflect: true,
      },

      speed: {
        // In px per second
        type: Number,
        reflect: true,
      },

      hidden: {
        type: Boolean,
        reflect: true,
      },

      fade: {
        type: Boolean,
        reflect: true,
      },

      contentWidth: {
        type: Number,
      },
    };
  }

  constructor() {
    super();
    this.direction = 'left'; // left or right
    this.speed = 150;
    this.contentWidth = 0;
    this._onSlotchange = this._onSlotchange.bind(this);
  }

  updated() {
    super.updated();
    const wrapper = this.shadowRoot.querySelector('.marquee-content');
    this.contentWidth = wrapper.getBoundingClientRect().width;
  }

  _onSlotchange() {
    this.requestUpdate();
  }

  get relativeDuration() {
    // convert speed (in pixel per second)
    // to relative duration
    return this.contentWidth / parseInt(this.speed);
  }

  render() {
    const classes = {
      marquee: true,
      fade: this.fade,
      hidden: this.hidden,
    };

    const isScrollLeft = this.direction === 'left';

    const styles = {
      animation: isScrollLeft ? 'marquee-scroll-left' : 'marquee-scroll-right',
      animationDuration: this.relativeDuration + 's',
      animationTimingFunction: 'linear',
      animationIterationCount: 'infinite',
    };

    if (!this.hidden)
      return html`
        <div class="${classMap(classes)}">
          ${this.fade
            ? html`
                <span class="gradient-left"></span>
                <span class="gradient-right"></span>
              `
            : null}
          <div style="${styleMap(styles)}" class="marquee-content">
            <slot @slotchange="${this._onSlotchange}"></slot>
          </div>
        </div>
      `;
  }
}

window.customElements.define('fw-marquee', Marquee);
