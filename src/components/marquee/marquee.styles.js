import { css } from 'lit';

export default css`
  :host {
    --marquee-background-color: #f9f9f9;
    --marquee-gradient: linear-gradient(
      to right,
      var(--marquee-background-color),
      rgba(255, 255, 255, 0)
    );
  }

  .marquee {
    width: 100%;
    max-width: 100%;
    overflow: hidden;
    background-color: var(--marquee-background-color);
    position: relative;
    display: flex;
    white-space: nowrap;
    box-sizing: border-box;
  }

  .marquee.hidden {
    display: none;
  }

  .marquee-content {
    display: flex;
    align-items: center;
    justify-content: left;
    min-width: max-content;
    padding-left: 100%;
  }

  .marquee.right .marquee-content {
    margin-right: -100%;
  }

  .gradient-left,
  .gradient-right {
    width: 200px;
    z-index: 2;
    height: 100%;
    position: absolute;
    top: 0;
    background: var(--marquee-gradient);
    pointer-events: none;
  }

  .gradient-left {
    left: 0;
  }

  .gradient-right {
    right: 0;
    transform: rotate(180deg);
  }

  .marquee:not(.fade) .gradient-left,
  .marquee:not(.fade) .gradient-right {
    display: none;
  }

  @keyframes marquee-scroll-left {
    from {
      transform: translate3d(0, 0, 0);
    }

    to {
      transform: translate3d(-100%, 0, 0);
    }
  }

  @keyframes marquee-scroll-right {
    from {
      transform: translate3d(-100%, 0, 0);
    }

    to {
      transform: translate3d(0, 0, 0);
    }
  }
`;
