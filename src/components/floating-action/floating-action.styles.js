import { css } from 'lit';

export default css`
  :host {
    --floating-action-background-color: #00a6ff;
    --floating-action-text-color: #fff;
    --floating-action-z-index: 100;
    --floating-action-left: unset;
    --floating-action-shadow: 0 2px 4px rgba(0, 0, 0, 0.08),
      0 4px 16px rgba(0, 0, 0, 0.08);
    --floating-action-margin: 2rem;
    position: fixed;
    inset: 0;
    margin: var(--floating-action-margin);
    display: flex;
    pointer-events: none;
    z-index: var(--floating-action-z-index);
  }

  .button.bottom-center {
    align-self: flex-end;
    margin-left: auto;
    margin-right: auto;
  }

  .button.bottom-left {
    align-self: flex-end;
    margin-left: 0;
    margin-right: auto;
  }

  .button.bottom-right {
    align-self: flex-end;
    margin-left: auto;
    margin-right: 0;
  }

  .button {
    background-color: var(--floating-action-background-color);
    right: var(--floating-action-right);
    border: none;
    border-radius: 1000px;
    height: 64px;
    max-width: 64px;
    transition: all 0.2s cubic-bezier(0.79, 0.14, 0.15, 0.86);
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 20px;
    z-index: var(--floating-action-z-index: 100);
    box-shadow: var(--floating-action-shadow);
    pointer-events: initial;
  }

  .button-icon {
    pointer-events: none;
    transition: all 0.2s cubic-bezier(0.79, 0.14, 0.15, 0.86);
    display: flex;
    width: 24px;
    min-height: 24px;
    min-width: 24px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    positon: realtive;
    z-index: 2;
  }

  .button:hover .button-icon {
    margin-right: 16px;
    margin-left: 8px;
  }

  .button-text {
    pointer-events: none;
    color: var(--floating-action-text-color);
    font-size: 18px;
    line-height: 1;
    white-space: nowrap;
    transition: all 0.2s cubic-bezier(0.79, 0.14, 0.15, 0.86);
    opacity: 0;
    display: inline-flex;
    align-items: center;
    max-width: 0;
    overflow: hidden;
    white-space: nowrap;
  }

  .button:hover .button-text {
    opacity: 1;
    position: relative;
    top: unset;
    left: unset;
    margin-right: 8px;
    max-width: 700px;
  }

  .button:hover {
    max-width: 700px;
  }
`;
