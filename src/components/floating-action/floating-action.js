import { html, LitElement } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import styles from './floating-action.styles.js';

export class FloatingAction extends LitElement {
  static get styles() {
    return styles;
  }

  static get properties() {
    return {
      position: {
        type: String, // 'bottom-left', 'bottom-right', 'bottom-center'
        reflect: true,
      },
    };
  }

  constructor() {
    super();
    this.position = 'bottom-right';
  }

  firstUpdated() {
    super.firstUpdated();
  }

  render() {
    const classes = {
      button: true,
      [this.position]: this.position,
    };

    return html` <button class="${classMap(classes)}">
      <span class="button-icon">
        <slot name="icon"></slot>
      </span>
      <span class="button-text">
        <slot></slot>
      </span>
    </button>`;
  }
}

window.customElements.define('fw-floating-action', FloatingAction);
