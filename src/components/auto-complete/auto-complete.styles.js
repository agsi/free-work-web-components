import { css } from 'lit';

export default css`
  :host {
    --border-color: #eee;
    --item-background: lavender;
    --mark-background: lavender;
    --mark-color: currentColor;
    --mark-font-weight: 500;
    --max-height: 380px;

    position: relative;
    display: inline-block;
    z-index: 1;
  }

  mark {
    background: var(--mark-background);
    color: var(--mark-color);
    font-weight: var(--mark-font-weight);
  }

  ul[role='listbox'] {
    position: absolute;
    top: 100%;
    left: 0;
    right: 0;
    list-style: none;
    background-color: white;
    padding: 0;
    margin: 0;
    max-height: var(--max-height);
    overflow-y: auto;
    border: 1px solid var(--border-color);
  }

  li[role='option'] {
    padding: 8px 16px;
    border-bottom: 1px solid var(--border-color);
  }

  [aria-selected='true'],
  [role='option']:hover {
    background-color: var(--item-background);
  }

  [aria-disabled='true'] {
    color: grey;
  }
`;
