// https://unpkg.com/@github/auto-complete-element@3.0.2/dist/bundle.js

import { html, LitElement } from 'lit';
import Combobox from '@github/combobox-nav';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
import debounce from '../../internals/debounce.js';
import styles from './auto-complete.styles.js';

function normalizeString(string) {
  /*
   * Converts a string to lower case & removes accents
   * Crème brulée —> creme brulee
   */
  return string
    .normalize('NFD')
    .replace(/\p{Diacritic}/gu, '')
    .toLowerCase()
    .trim();
}

function highlightMatchingParts(word, input) {
  /*
   * Compares a string with a given input & highlights only the parts
   * of the string that matches a given keyword or serie of keywords
   *
   * eg:
   * word ——————————————————— Développeur
   * user input ————————————— Vue.js deve
   * output ———————————————————————— Développeur
   *                                 |  |
   *                                 |__|__ only the matching part is highlighted
   */

  // We normalize the user input and the word we compare it to
  // because accents and casing differences should be ignored.
  // Note: the user input is an array because it can either
  // be a word or a serie of keywords
  const normalizedWord = normalizeString(word);
  const normalizedKeywords = normalizeString(input).split(' ');

  const foundMatch = normalizedKeywords.find(input =>
    normalizedWord.includes(input)
  );

  if (foundMatch) {
    const firstChar = foundMatch.charAt(0);
    const lastCharIndex = foundMatch.length - 1;
    const firstCharIndex = normalizedWord.indexOf(firstChar);
    const match = word.slice(firstCharIndex, lastCharIndex + 1);
    return word.replace(new RegExp(`(${match})`, 'i'), '<mark>$1</mark>');
  }

  return word;
}
export class AutoComplete extends LitElement {
  static get styles() {
    return styles;
  }

  static get properties() {
    return {
      /**
       * Optional endpoint to dynamically fetch suggestions items.
       * Example: `/api/geo_autocomplete?search={query}`
       */
      src: {
        type: String,
      },

      /**
       * Optional finite list of suggestions items.
       */
      items: {
        type: Array,
      },

      for: {
        type: String,
      },

      open: {
        type: Boolean,
        reflect: true,
      },

      /**
       * The label json path from item Object, that will be visible in the listbox.
       */
      label: {
        type: String,
      },

      /**
       * Optional json path to extract items from fetch response.
       */
      itemsJsonPath: {
        type: String,
      },

      clearOnSelect: {
        type: Boolean,
      },

      noHighlight: {
        type: Boolean,
      },

      value: {
        type: String,
        reflect: true,
      },

      letterCountThreshold: {
        type: Number,
        reflect: true,
      },
    };
  }

  constructor() {
    // Always call super() first
    super();

    // Initialize properties
    this.src = '';
    this.for = '';
    this.open = false;
    this.noHighlight = false;
    this.value = '';
    this.items = [];
    this.currentItems = this.items;

    // Initialize events listeners
    this.onInputChange = debounce(this.onInputChange.bind(this), 500);
    this.onInputFocus = this.onInputFocus.bind(this);
    this.onInputBlur = this.onInputBlur.bind(this);
    this.onKeydown = this.onKeydown.bind(this);
    this.onCommit = this.onCommit.bind(this);
    this.onListMouseDown = this.onListMouseDown.bind(this);

    this.requests = new WeakMap();
    this.interactingWithList = false;
  }

  firstUpdated() {
    super.firstUpdated();
    this.comboBox = new Combobox(this.input, this.listElement);

    this.input.setAttribute('autocomplete', 'off');
    this.input.setAttribute('spellcheck', 'false');
    this.input.addEventListener('input', this.onInputChange);
    this.input.addEventListener('keydown', this.onKeydown);
    this.input.addEventListener('focus', this.onInputFocus);
    this.input.addEventListener('blur', this.onInputBlur);
    this.listElement.addEventListener('mousedown', this.onListMouseDown);
    this.listElement.addEventListener('combobox-commit', this.onCommit);
  }

  disconnectedCallback() {
    this.input.removeEventListener('input', this.onInputChange);
    this.input.removeEventListener('keydown', this.onKeydown);
    this.input.removeEventListener('focus', this.onInputFocus);
    this.input.removeEventListener('blur', this.onInputBlur);
    this.listElement.removeEventListener('mousedown', this.onListMouseDown);
    this.listElement.removeEventListener('combobox-commit', this.onCommit);
    super.disconnectedCallback();
  }

  get input() {
    return this.shadowRoot
      .querySelector('slot')
      .assignedElements({ flatten: true })
      .reduce(
        (input, el) =>
          el.nodeName === 'INPUT' ? el : el.querySelector('input'),
        null
      );
  }

  get listElement() {
    return this.shadowRoot.querySelector('[role="listbox"]');
  }

  get standalone() {
    return Array.isArray(this.items) && this.items.length > 0;
  }

  onCommit({ target }) {
    const selected = target;
    if (!(selected instanceof HTMLElement)) return;
    this.open = false;
    if (selected instanceof HTMLAnchorElement) return;
    const label = selected.textContent.trim();
    const item = this.currentItems[parseInt(selected.dataset.index)];

    this._fireEvent('change', { label, item });
    this.input.value = this.clearOnSelect ? '' : label;
  }

  onKeydown(event) {
    if (event.key === 'Escape' && this.open) {
      this.open = false;
      event.stopPropagation();
      event.preventDefault();
    } else if (event.altKey && event.key === 'ArrowUp' && this.open) {
      this.open = false;
      event.stopPropagation();
      event.preventDefault();
    } else if (event.altKey && event.key === 'ArrowDown' && !this.open) {
      if (!this.input.value.trim()) return;
      this.open = true;
      event.stopPropagation();
      event.preventDefault();
    }
  }

  onListMouseDown() {
    this.interactingWithList = true;
  }

  onInputFocus() {
    if (this.standalone) {
      this.fetchItems();
    }
  }

  onInputBlur() {
    if (this.interactingWithList) {
      this.interactingWithList = false;
      return;
    }
    this.hide();
  }

  onInputChange() {
    this.fetchItems();
  }

  fetchItems() {
    const value = this.input.value.trim();

    if (this.standalone) {
      const normalizedInput = normalizeString(value);

      this.currentItems = this.items.filter(item => {
        const normalizedLabel = normalizeString(item[this.label]);
        const keywordsArray = normalizedInput.split(' ');
        const multipleKeywords = keywordsArray.length > 1;

        if (multipleKeywords) {
          return keywordsArray.every(keyword =>
            normalizedLabel.includes(keyword)
          );
        }

        return normalizedLabel.includes(normalizedInput);
      });

      this.show();
      return;
    }

    const threshold =
      this.letterCountThreshold !== undefined ? this.letterCountThreshold : 2;

    if (value.length < threshold) {
      // this.hide();
      return;
    }

    const pending = this.requests.get(this.input);
    if (pending) {
      pending.abort();
    }

    const abortController = new AbortController();

    fetch(this.src.replace('{query}', value), {
      method: 'GET',
      signal: abortController.signal,
      headers: {
        'X-Varnish-Public': '1',
      },
    })
      .then(res => res.json())
      .then(data => {
        this.currentItems = this.itemsJsonPath
          ? this.itemsJsonPath.split('.').reduce((acc, x) => acc[x], data)
          : data;

        this._fireEvent('after-fetch', { items: this.currentItems });
        this.requests.delete(this.input);
        this.show();
      })
      .catch(err => {
        this._fireEvent('fetch-error', { err });
      });

    this.requests.set(this.input, abortController);
  }

  updated(changedProperties) {
    if (
      changedProperties.has('open') &&
      changedProperties.get('open') === undefined
    ) {
      // Skip first update
      // changedProperties gives the property values from the previous update.
      // At the time of the first update, previous values are always undefined.
      return;
    }

    if (changedProperties.has('open')) {
      if (this.open) {
        this.show();
      } else {
        this.hide();
      }
    }
    if (changedProperties.has('value')) {
      this.input.value = this.value;
    }
  }

  show() {
    if (this.open) {
      this.requestUpdate();
      return;
    }
    this.comboBox.start();
    this.open = true;
  }

  hide() {
    if (!this.open) return;
    this.comboBox.stop();
    this.open = false;
  }

  focus() {
    this.input.focus();
  }

  _fireEvent(name, detail) {
    return this.dispatchEvent(
      new CustomEvent(name, {
        detail,
        bubbles: true,
        composed: true,
      })
    );
  }

  _highlightValueIntoString(text) {
    if (this.noHighlight) {
      return text;
    }

    // Highlight matching parts for each word
    const keywords = this.input.value;
    const words = text.split(' ');
    const highlightedWords = words.map(word =>
      highlightMatchingParts(word, keywords)
    );

    return unsafeHTML(highlightedWords.join(' '));
  }

  onSlotChange(e) {}

  render() {
    let id = 0;
    return html`<slot @slotchange=${this.onSlotChange}></slot>
      <ul for="${this.for}" role="listbox" part="list" .hidden="${!this.open}">
        ${this.open
          ? this.currentItems.map(
              (item, index) =>
                html`<li
                  id=${`option-${id++}`}
                  role="option"
                  data-index="${index}"
                >
                  ${this._highlightValueIntoString(item[this.label])}
                </li>`
            )
          : ''}
      </ul>`;
  }
}

window.customElements.define('fw-auto-complete', AutoComplete);
