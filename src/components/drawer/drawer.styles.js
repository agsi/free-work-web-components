import { css } from 'lit';

export default css`
  :host {
    pointer-events: none;
    --drawer-width: clamp(
      320px,
      400px,
      var(--window-width)
    ); /* Only used when placement is set to 'left' or 'right'*/
    --drawer-height: var(
      --window-height
    ); /* Only used when placement is set to 'top' or 'bottom'*/
    --drawer-bg-color: #ffffff;
    --overlay-color: rgba(0, 0, 0, 0.4);
    --drawer-anim-duration: 0.4s;
    --drawer-easing: cubic-bezier(0.23, 1, 0.32, 1);
    --drawer-content-padding: 0 18px;
    --drawer-head-padding: 18px;
    --drawer-foot-padding: 18px;
    --drawer-z-index: 999999;
  }

  :host,
  :host * {
    box-sizing: border-box;
  }

  .drawer {
    position: fixed;
    display: flex;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: var(--drawer-z-index);
  }

  .drawer.right {
    justify-content: flex-end;
  }

  .drawer.bottom {
    align-items: flex-end;
  }

  .drawer.right .drawer-inner {
    transform: translate3d(100%, 0, 0);
  }

  .drawer.left .drawer-inner {
    transform: translate3d(-100%, 0, 0);
  }

  .drawer.top .drawer-inner {
    transform: translate3d(0, -100%, 0);
  }

  .drawer.bottom .drawer-inner {
    transform: translate3d(0, 100%, 0);
  }

  .drawer-overlay {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    opacity: 0;
    background-color: var(--overlay-color);
    transition: all var(--drawer-anim-duration) var(--drawer-easing);
  }

  .drawer-inner {
    z-index: 1;
    display: flex;
    position: relative;
    pointer-events: auto;
    flex-direction: column;
    background-color: var(--drawer-bg-color);
    transition: transform var(--drawer-anim-duration) var(--drawer-easing);
  }

  .drawer.right .drawer-inner,
  .drawer.left .drawer-inner {
    width: var(--drawer-width);
    height: var(--drawer-height);
    max-height: var(--drawer-height);
  }

  .drawer.top .drawer-inner,
  .drawer.bottom .drawer-inner {
    width: var(--window-width);
    height: fit-content;
  }

  .drawer-content {
    height: var(--drawer-height);
    overflow-y: auto;
    padding: var(--drawer-content-padding);
  }

  .drawer.active .drawer-inner {
    transform: none;
  }

  .drawer.active .drawer-overlay {
    opacity: 1;
  }

  .drawer-close-button {
    border: none;
    padding: 0;
    margin: 0;
    margin-left: auto;
    background: none;
    cursor: pointer;
  }

  .drawer-head {
    display: flex;
    flex: none;
    align-items: center;
    padding: var(--drawer-head-padding);
  }

  .drawer-foot {
    padding-top: var(--drawer-gap);
    margin-top: auto;
    padding: var(--drawer-foot-padding);
  }
`;
