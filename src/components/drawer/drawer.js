import { html, LitElement } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { dispatchEvent } from '../../internals/event';
import styles from './drawer.styles';
import './burger-button.js';
export class Drawer extends LitElement {
  static get styles() {
    return styles;
  }

  static get properties() {
    return {
      closeable: {
        type: Boolean,
        reflect: true,
      },

      'close-on-click-outside': {
        type: Boolean,
        reflect: true,
      },

      'close-on-escape': {
        type: Boolean,
        reflect: true,
      },

      placement: {
        type: String,
        reflect: true,
      },

      active: {
        type: Boolean,
        reflect: true,
      },
    };
  }

  constructor() {
    // Always call super() first
    super();

    this.active = false;
    this.placement = 'right'; // 'right' | 'left' | 'top' | 'bottom'
    this.closeable = false;
    this['close-on-escape'] = true;
    this['close-on-click-outside'] = true;
    this._onClickOutside = this._onClickOutside.bind(this);
    this.open = this.open.bind(this);
    this.footerHasChildren = false;
    this._onResize = this._onResize.bind(this);
  }

  connectedCallback() {
    super.connectedCallback();
    window.addEventListener('resize', this._onResize);
    this._onResize();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    window.removeEventListener('resize', this._onResize);
  }

  _onResize() {
    this.style.setProperty('--window-height', `${window.innerHeight}px`);
    this.style.setProperty('--window-width', `${window.innerWidth}px`);
  }

  firstUpdated() {
    const slot = this.shadowRoot.querySelector('.drawer-foot slot');
    this.footerHasChildren = !!slot.assignedElements().length;
    super.firstUpdated();
  }

  // eslint-disable-next-line class-methods-use-this
  _disableBodyScroll() {
    const { body } = document;
    body.dataset.scrollY = window.scrollY;
    body.style.position = 'fixed';
    body.style.inset = '0';
    body.style.top = `-${body.dataset.scrollY}px`;
  }

  _onKeyDown({ key }) {
    if (this['close-on-escape'] && key === 'Escape') {
      this.close();
    }
  }

  // eslint-disable-next-line class-methods-use-this
  _enableBodyScroll() {
    const { body } = document;
    body.style.position = '';
    body.style.top = '';
    body.style.inset = '';
    window.scrollTo(0, Number(body.dataset.scrollY));
  }

  _onClickOutside() {
    if (!this.active || !this['close-on-click-outside']) return;
    this.close();
  }

  _onClickInside(e) {
    e.stopPropagation();
  }

  open() {
    if (this.active) return;
    this._onOpen();
  }

  close() {
    if (!this.active) return;
    this._onClose();
  }

  _onOpen() {
    this.active = true;
    this._disableBodyScroll();
    this.requestUpdate();
    dispatchEvent(this, 'open');
  }

  _onClose() {
    this.active = false;
    this._enableBodyScroll();
    dispatchEvent(this, 'close');
  }

  _updateListeners() {
    if (this.active) {
      this.addEventListener('click', this._onClickInside);
      document.addEventListener('click', this._onClickOutside);
      document.addEventListener('keydown', this._onKeyDown);
      return;
    }
    this.removeEventListener('click', this._onClickInside);
    document.removeEventListener('click', this._onClickOutside);
    document.removeEventListener('keydown', this._onKeyDown);
  }

  updated(changedProperties) {
    if (!changedProperties.has('active')) return;
    if (this.active) {
      this._onOpen();
    } else {
      this._onClose();
    }
  }

  render() {
    const classes = {
      drawer: true,
      active: this.active,
      [this.placement]: this.placement,
    };

    const closeButton = html`<fw-burger-button
      @click="${this.close}"
      .active="${this.active}"
      class="drawer-close-button"
    >
      Close
    </fw-burger-button>`;

    const footer = html`
      <footer
        class="drawer-foot"
        .hidden="${this.footerHasChildren ? null : 'true'}"
      >
        <slot name="footer"></slot>
      </footer>
    `;

    return html`
      <div
        class="${classMap(classes)}"
        aria-hidden=${this.active ? 'false' : 'true'}
      >
        <div class="drawer-overlay"></div>
        <div @transitionend="${this._updateListeners}" class="drawer-inner">
          <header class="drawer-head">
            <slot name="head"></slot>
            ${this.closeable ? closeButton : null}
          </header>
          <div class="drawer-content">
            <slot></slot>
          </div>
          ${footer}
        </div>
      </div>
    `;
  }
}

window.customElements.define('fw-drawer', Drawer);
