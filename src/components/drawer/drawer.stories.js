import { html } from 'lit';
import './burger-button.js';
import './drawer.js';

export default {
  title: 'Core/fw-drawer',
  argTypes: {
    placement: {
      control: {
        type: 'inline-radio',
        options: ['left', 'right', 'top', 'bottom'],
      },
      defaultValue: 'right',
    },
  },
};

const Template = args => html`
  <div class="w-full h-screen">
    <fw-drawer
      .active="${args.active}"
      .closeable="${args.closeable}"
      .placement="${args.placement}"
      .close-on-escape="${args['close-on-escape']}"
      .close-on-click-outside="${args['close-on-click-outside']}"
    >
      <span slot="head" class="font-semibold text-xl">Head</span>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
        consequat dignissim nibh. Aenean auctor euismod nisl, nec molestie
        risus. Nam mattis tempus elit, in gravida odio ultricies eu.
        Pellentesque vitae ante molestie, bibendum tellus eget, faucibus justo.
        Maecenas velit metus, ullamcorper id eleifend sed, egestas ut orci. Orci
        varius natoque penatibus et magnis dis parturient montes, nascetur
        ridiculus mus. Fusce in hendrerit justo.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
        consequat dignissim nibh. Aenean auctor euismod nisl, nec molestie
        risus. Nam mattis tempus elit, in gravida odio ultricies eu.
        Pellentesque vitae ante molestie, bibendum tellus eget, faucibus justo.
        Maecenas velit metus, ullamcorper id eleifend sed, egestas ut orci. Orci
        varius natoque penatibus et magnis dis parturient montes, nascetur
        ridiculus mus. Fusce in hendrerit justo.
      </p>

      <div slot="footer" class="flex justify-between items-center">
        <p>Drawer footer</p>

        <button
          id="drawer-close-button"
          class="px-4 py-2 bg-gray-100 border rounded text-gray-600"
          type="button"
        >
          Close
        </button>
      </div>
    </fw-drawer>

    <div class="fixed top-0 right-0 mt-5 mr-5">
      <fw-burger-button id="burger-button"></fw-burger-button>
    </div>

    <script>
      const drawer = document.querySelector('fw-drawer');
      const burgerButton = document.querySelector('#burger-button');
      const closeButton = document.querySelector('#drawer-close-button');

      closeButton.addEventListener('click', () => {
        drawer.close();
      });

      burgerButton.addEventListener('click', () => {
        if (burgerButton.getAttribute('active')) {
          burgerButton.removeAttribute('active');
          close.open();
        } else {
          burgerButton.setAttribute('active', true);
          drawer.open();
        }
      });

      drawer.addEventListener('close', () => {
        burgerButton.removeAttribute('active');
      });
    </script>
  </div>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  active: false,
  closeable: true,
  'close-on-escape': true,
  'close-on-click-outside': true,
};
