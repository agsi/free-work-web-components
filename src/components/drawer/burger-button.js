import { html, css, LitElement } from 'lit';
import { classMap } from 'lit/directives/class-map.js';

export class BurgerButton extends LitElement {
  static get styles() {
    return css`
      :host {
        --menu-burger-color: currentColor;
        --menu-burger-anim-easing: cubic-bezier(0.86, 0, 0.07, 1);
        --menu-burger-anim-duration: 0.4s;
      }
      .menu-burger {
        background: transparent;
        border: none;
        color: inherit;
        position: relative;
        cursor: pointer;
        width: 48px;
        height: 48px;
        padding: 0;
        margin: 0;
      }
      .menu-burger span {
        position: absolute;
        height: 2px;
        width: 36px;
        left: 6px;
        display: block;
        background-color: var(--menu-burger-color);
        transition: all var(--menu-burger-anim-duration)
          var(--menu-burger-anim-easing);
        pointer-events: none;
      }
      .menu-burger span:nth-child(1) {
        top: 10px;
      }
      .menu-burger span:nth-child(2) {
        top: 22px;
      }
      .menu-burger span:nth-child(3) {
        top: 34px;
      }
      .menu-burger.active span:nth-child(1) {
        transform: translateY(12px) rotate(-45deg);
      }
      .menu-burger.active span:nth-child(2) {
        opacity: 0;
        transform: rotate(-45deg);
      }
      .menu-burger.active span:nth-child(3) {
        transform: translateY(-12px) rotate(45deg);
      }
    `;
  }

  static get properties() {
    return {
      active: {
        type: Boolean,
        reflect: true,
      },
    };
  }

  constructor() {
    // Always call super() first
    super();
    this.active = false;
  }

  render() {
    const classes = {
      'menu-burger': true,
      active: this.active,
    };

    return html`
      <button class="${classMap(classes)}" type="button">
        <span></span>
        <span></span>
        <span></span>
      </button>
    `;
  }
}

window.customElements.define('fw-burger-button', BurgerButton);
