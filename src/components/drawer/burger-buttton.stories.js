import { html } from 'lit';
import './burger-button.js';
import './drawer.js';

export default {
  title: 'Core/fw-burger-button',
  argTypes: {
    placement: {
      control: {
        type: 'inline-radio',
        options: ['left', 'right'],
      },
      defaultValue: 'right',
    },
  },
};

function toggleBurgerButton({ target }) {
  if (target.getAttribute('active')) {
    target.removeAttribute('active');
  } else {
    target.setAttribute('active', true);
  }
}

const Template = args => html`
  <div class="w-full h-screen grid place-items-center">
    <div>
      <fw-burger-button
        .active="${args.active}"
        @click="${toggleBurgerButton}"
      ></fw-burger-button>
    </div>
  </div>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  active: false,
};
