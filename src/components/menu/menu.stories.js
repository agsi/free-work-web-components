import { html } from 'lit';
import './menu.js';
import './menu-item.js';
import '../drawer/drawer';

export default {
  title: 'Core/fw-menu',
  argTypes: {},
};

const menuItems = [
  {
    label: 'Pomegranate',
    href: '/',
  },
  {
    label: 'Grapefruit',
    href: '/',
  },
  {
    label: 'Tangerine',
    subMenu: [
      {
        label: 'Peach',
        href: '/',
      },
      {
        label: 'Blackberry',
        href: '/',
      },
      {
        label: 'Guava',
        href: '/',
      },
      {
        label: 'Passion fruit',
        href: '/',
      },
    ],
  },
  {
    label: 'Mango',
    href: '/',
  },
  {
    label: 'Avocado',
    href: '/',
  },
  {
    label: 'Apricot',
    subMenu: [
      {
        label: 'Strawberry',
        href: '/',
      },
      {
        label: 'Coconut',
        href: '/',
      },
      {
        label: 'Jujube',
        href: '/',
      },
      {
        label: 'Banana',
        href: '/',
      },
      {
        label: 'Grapefruit',
        href: '/',
      },
      {
        label: 'Star fruit',
        href: '/',
      },
    ],
  },
  {
    label: 'Honeydew',
    href: '/',
  },
  {
    label: 'Cherimoya',
    href: '/',
  },
  {
    label: 'Lemon',
    href: '/',
  },
  {
    label: 'Longan',
    href: '/',
  },
  {
    label: 'Horned melon',
    href: '/',
  },
  {
    label: 'Pineapple',
    href: '/',
  },
];

const Template = args => html`
  <div class="w-full h-screen grid place-items-center">
    <fw-drawer style="--drawer-content-padding: 0" .active="${args.active}">
      <div slot="head" class="text-xl font-bold">TheCompany™</div>
      <fw-menu>
        ${menuItems.map(
          item => html`
            <fw-menu-item
              ?has-children="${item.subMenu}"
              label="${item.label}"
              .href="${item.href}"
            >
              ${item.subMenu
                ? item.subMenu.map(
                    subItem => html`
                      <fw-menu-item
                        ?has-children="${subItem.subMenu}"
                        label="${subItem.label}"
                        .href="${subItem.href}"
                      ></fw-menu-item>
                    `
                  )
                : null}
            </fw-menu-item>
          `
        )}
      </fw-menu>

      <div slot="footer" class="text-sm text-gray-500 flex justify-between">
        <span class="font-bold"> TheFooter™ </span>
        <span> ©Free-Work 2k22 </span>
      </div>
    </fw-drawer>
  </div>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  active: true,
};
