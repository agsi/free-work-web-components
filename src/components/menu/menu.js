import { html, css, LitElement } from 'lit';

export class Menu extends LitElement {
  static get styles() {
    return css`
      .menu {
        list-style-type: none;
        margin: 0;
        padding: 0;
      }
    `;
  }

  static get properties() {
    return {
      active: {
        type: Boolean,
        reflect: true,
      },
    };
  }

  constructor() {
    // Always call super() first
    super();
    this.active = false;
  }

  render() {
    return html`
      <ul class="menu">
        <slot></slot>
      </ul>
    `;
  }
}

window.customElements.define('fw-menu', Menu);
