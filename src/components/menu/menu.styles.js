import { html, LitElement } from 'lit';
import styles from './menu.styles';

export class Menu extends LitElement {
  static get styles() {
    return styles;
  }

  static get properties() {
    return {
      active: {
        type: Boolean,
        reflect: true,
      },
    };
  }

  constructor() {
    // Always call super() first
    super();
    this.active = false;
  }

  render() {
    return html`
      <ul class="menu">
        <slot></slot>
      </ul>
    `;
  }
}

window.customElements.define('fw-menu', Menu);
