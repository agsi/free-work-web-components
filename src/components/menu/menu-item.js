import { html, css, LitElement } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { dispatchEvent } from '../../internals/event';

export class MenuItem extends LitElement {
  static get styles() {
    return css`
      :host {
        font-size: 1.15em;
        --menu-item-color: #545454;
        --menu-divider-color: rgba(0, 0, 0, 0.08);
        --sub-menu-divider-color: var(--menu-divider-color);
        --sub-menu-background-color: #f1f1f1;
        --sub-menu-indent: 1.5em;
        --menu-item-padding: 1em 1.125em;
      }

      :host > .menu-item:not(.is-first) {
        border-top: solid 1px var(--menu-divider-color);
      }

      :host,
      :host * {
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
      }

      .menu-item {
        line-height: 1.5;
        cursor: pointer;
        transition: all 0.3s;
        color: var(--menu-item-color);
      }

      .menu-item a {
        text-decoration: none;
        color: inherit;
      }

      .menu-item .sub-menu-arrow {
        height: 1.5em;
        height: 1.5em;
        transition: all 0.3s;
        opacity: 0.5;
        path {
          fill: currentColor;
        }
      }

      .menu-item > *:first-child {
        display: block;
        padding: var(--menu-item-padding);
      }

      .menu-item.has-children > *:first-child {
        display: flex;
        align-items: center;
        justify-content: space-between;
      }

      .menu-item.has-children.active .sub-menu-arrow {
        transform: rotate(180deg);
      }

      .sub-menu {
        list-style-type: none;
        margin: 0;
        padding: 0;
      }

      .sub-menu slot::slotted(*) {
        font-size: 0.8em;
      }

      .sub-menu {
        overflow: hidden;
        transition: all 0.4s cubic-bezier(0.19, 1, 0.22, 1);
        background-color: var(--sub-menu-background-color);
      }

      .menu-item.is-child {
        display: block;
        padding-left: var(--sub-menu-indent);
      }

      .menu-item.is-child:not(.is-first) {
        border-top: solid 1px var(--sub-menu-divider-color) !important;
      }

      .sub-menu slot::slotted(*:last-child) {
        border-top: none;
      }
    `;
  }

  static get properties() {
    return {
      label: {
        type: String,
        reflect: true,
      },
      href: {
        type: String,
        reflect: true,
      },
      active: {
        type: Boolean,
        reflect: true,
      },
      isFirst: {
        type: Boolean,
        reflect: true,
        attribute: 'is-first',
      },
      isLast: {
        type: Boolean,
        reflect: true,
        attribute: 'is-last',
      },
      isChild: {
        type: Boolean,
        reflect: true,
        attribute: 'is-child',
      },
      hasChildren: {
        type: Boolean,
        reflect: false,
        attribute: 'has-children',
      },
      preventLinks: {
        type: Boolean,
        reflect: true,
        attribute: 'prevent-links',
      },
    };
  }

  constructor() {
    // Always call super() first
    super();
    this.active = false;
    this.hasChildren = false;
    this.subMenuHeight = 0;
  }

  _onClick() {
    dispatchEvent(this, 'click');
    if (!this.hasChildren) return;
    this._getSubElementsHeight();
    this.active = !this.active;
  }

  _onLinkClick(e) {
    if (!this.preventLinks) return;
    e.preventDefault();
  }

  _stopPropagation(e) {
    e.stopPropagation();
  }

  _getSlottedElements() {
    const slot = this.shadowRoot.querySelector('.sub-menu slot');
    const slotElements = slot ? slot.assignedElements() : [];
    return slotElements;
  }

  _getSubElementsHeight() {
    const slotElements = this._getSlottedElements();
    if (!slotElements.length) return;
    // Get sub-menu items total height
    this.subMenuHeight = slotElements.reduce((height, el) => {
      return height + el.getBoundingClientRect().height;
    }, 0);
  }

  firstUpdated() {
    super.firstUpdated();
    this.isFirst = !this.previousElementSibling;
    this.isLast = !this.nextElementSibling;
    const slotElements = this._getSlottedElements();
    slotElements.forEach(item => {
      item.setAttribute('is-child', true);
    });
  }

  render() {
    const classes = {
      'menu-item': true,
      active: this.active,
      'has-children': this.hasChildren,
      'is-child': this.isChild,
      'is-first': this.isFirst,
      'is-last': this.isLast,
    };

    const arrowIcon = html`<svg
      class="sub-menu-arrow"
      xmlns="http://www.w3.org/2000/svg"
      height="24px"
      viewbox="0 0 24 24"
      width="24px"
      fill="#000000"
    >
      <path d="M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z"></path>
    </svg>`;

    const linkLabel = html`
      <a @click="${this._onLinkClick}" href="${this.href}" class="label">
        ${this.label}
        <slot name="label"></slot>
        ${this.hasChildren ? arrowIcon : null}
      </a>
    `;

    const label = html`
      <span class="label">
        ${this.label}
        <slot name="label"></slot>
        ${this.hasChildren ? arrowIcon : null}
      </span>
    `;

    return html`
      <li @click="${this._onClick}" class="${classMap(classes)}">
        ${this.href ? linkLabel : label}
        ${this.hasChildren
          ? html` <ul
              style="height: ${this.active ? this.subMenuHeight : 0}px"
              @click="${this._stopPropagation}"
              class="sub-menu"
            >
              <slot></slot>
            </ul>`
          : null}
      </li>
    `;
  }
}

window.customElements.define('fw-menu-item', MenuItem);
