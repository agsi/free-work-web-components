// yarn add intl-tel-input
// https://github.com/jackocnr/intl-tel-input

import { html, LitElement } from 'lit';
import intlTelInput from 'intl-tel-input';
import { dispatchEvent } from '../../internals/event.js';

export class InputTel extends LitElement {
  constructor() {
    // Always call super() first
    super();

    // Initialize events listeners
    this.onInput = this.onInput.bind(this);
  }

  get input() {
    return this.shadowRoot
      .querySelector('slot')
      .assignedElements({ flatten: true })
      .reduce(
        (input, el) =>
          el.nodeName === 'INPUT' ? el : el.querySelector('input'),
        null
      );
  }

  firstUpdated() {
    this.iti = intlTelInput(this.input, {
      initialCountry: 'fr',
      autoPlaceholder: 'polite',
      // For formatting placeholders
      utilsScript:
        'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.min.js',
    });

    this.input.addEventListener('input', this.onInput);
  }

  disconnectedCallback() {
    this.input.removeEventListener('input', this.onInput);
    super.disconnectedCallback();
  }

  onInput(event) {
    event.stopPropagation();

    dispatchEvent(this, 'input', {
      name: this.input.name || '',
      // E.164 format: +330612345678
      value: this.iti.getNumber(),
      countryData: {
        ...this.iti.getSelectedCountryData(),
      },
      // https://github.com/jackocnr/intl-tel-input/blob/master/src/js/utils.js#L119
      type: this.iti.getNumberType(),
      isValid: this.iti.isValidNumber(),
      error: this.iti.getValidationError(),
    });
  }

  render() {
    return html`<slot />`;
  }
}

window.customElements.define('fw-input-tel', InputTel);
