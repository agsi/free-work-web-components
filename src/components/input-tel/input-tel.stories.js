import { html } from 'lit';
import './input-tel.js';

export default {
  title: 'Core/fw-input-tel',
  argTypes: {
    onInput: { action: 'input' },
  },
};

const Template = args => html`
  <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/css/intlTelInput.css"
  />
  <fw-input-tel @input="${args.onInput}">
    <input class="border rounded py-2" type="tel" name="tel" />
  </fw-input-tel>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {};
