import { css } from 'lit';

export const timelineStyles = css`
  :host {
    --timeline-line-color: #ececec;
    --timeline-completed-line-color: #002c46;
    --timeline-step-width: 110px;
    --timeline-line-color: #ececec;
    --timeline-line-stroke-width: 4px;
    --timeline-icon-wrapper-size: 40px;
    --timeline-icon-wrapper-height: var(--timeline-icon-wrapper-size);
    --timeline-icon-wrapper-width: var(--timeline-icon-wrapper-size);
    --timeline-z-index: 2;
    position: relative;
    z-index: var(--timeline-z-index);
    display: flex;
    width: min-content;
    max-width: 100%;
  }

  .timeline {
    display: inline-flex;
    position: relative;
    width: max-content;
  }

  @media screen and (max-width: 768px) {
    :host {
      width: 100%;
    }
    .timeline {
      flex-direction: column;
      width: 100%;
    }
    .line {
      display: none;
    }
  }

  ::slotted(fw-timeline-step) {
    z-index: 2;
  }

  @media screen and (min-width: 768px) {
    :host {
      width: 100%;
    }
    .line {
      position: absolute;
      left: calc(var(--timeline-step-width) / 2);
      top: calc(
        (var(--timeline-icon-wrapper-height) / 2) -
          (var(--timeline-line-stroke-width) / 2)
      );
      height: var(--timeline-line-stroke-width);
      background-color: var(--timeline-line-color);
      width: calc(100% - var(--timeline-step-width));
      z-index: -1;
    }
  }
`;

export const timelineStepStyles = css`
  :host {
    position: relative;
  }

  .timeline-step {
    width: var(--timeline-step-width);
    text-align: center;
    position: relative;
  }

  .timeline-step.completed .timeline-step-icon {
    position: relative;
    z-index: 3;
  }

  @media screen and (min-width: 800px) {
    .timeline-step.completed .timeline-step-icon:after {
      content: '';
      display: block;
      height: var(--timeline-line-stroke-width);
      background-color: var(--timeline-completed-line-color);
      width: calc(
        var(--timeline-step-width) - var(--timeline-icon-wrapper-width) / 2
      );
      position: absolute;
      top: calc(
        (var(--timeline-icon-wrapper-height) / 2) -
          (var(--timeline-line-stroke-width) / 2)
      );
      right: 50%;
      z-index: -1;
      pointer-events: none;
    }

    :host(:first-child) .timeline-step.completed .timeline-step-icon:after {
      display: none;
    }
  }

  .timeline-step-icon {
    position: relative;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    height: var(--timeline-icon-wrapper-height);
    width: var(--timeline-icon-wrapper-width);
  }

  .timeline-step-icon-wrapper {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .timeline-step-label {
    margin-top: 4px;
  }

  @media screen and (max-width: 800px) {
    .timeline-step {
      width: 100%;
      text-align: left;
      display: flex;
      align-items: center;
      margin-bottom: 24px;
    }
    .timeline-step-label {
      margin-top: 0;
    }
    .timeline-content {
      width: 100%;
      padding-left: 16px;
    }
  }
`;
