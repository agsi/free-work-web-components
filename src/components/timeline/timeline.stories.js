import { html } from 'lit';
import './timeline.js';

export default {
  title: 'Core/fw-timeline',
  argTypes: {
    'step-width': {
      control: {
        type: 'range',
        step: 1,
        min: 40,
        max: 200,
      },
    },
    'line-stroke-width': {
      control: {
        type: 'range',
        step: 1,
        min: 1,
        max: 20,
      },
    },
    'icon-wrapper-size': {
      control: {
        type: 'range',
        step: 1,
        min: 1,
        max: 100,
      },
    },
    'line-color': {
      control: {
        type: 'color',
      },
    },
  },
};

const Template = args => html`
  <div class="p-4 md:pt-20 w-full h-full flex items-center justify-center">
    <div class="w-full md:w-auto">
      <fw-timeline
        step-width="${args['step-width']}"
        line-stroke-width="${args['line-stroke-width']}"
        line-color="${args['line-color']}"
        icon-wrapper-size="${args['icon-wrapper-size']}"
      >
        <fw-timeline-step completed>
          <span
            slot="icon"
            class="flex items-center justify-center w-10 h-10 bg-blue-600 rounded-full text-white"
          >
            <svg
              width="20"
              height="20"
              aria-hidden="true"
              focusable="false"
              data-prefix="fad"
              data-icon="acorn"
              class="svg-inline--fa fa-acorn fa-w-14"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 448 512"
            >
              <g class="fa-group">
                <path
                  class="fa-secondary"
                  fill="currentColor"
                  d="M32 256h384a258.87 258.87 0 0 1-143.11 231.55L224 512l-48.89-24.45A258.87 258.87 0 0 1 32 256z"
                  opacity="0.4"
                ></path>
                <path
                  class="fa-primary"
                  fill="currentColor"
                  d="M448 160v32a32 32 0 0 1-32 32H32a32 32 0 0 1-32-32v-32a96 96 0 0 1 96-96h106a132.41 132.41 0 0 1 29.41-58.64 15.7 15.7 0 0 1 11.31-5.3 15.44 15.44 0 0 1 12 4.72L266 16.1a16 16 0 0 1 .66 21.9 84.32 84.32 0 0 0-15.16 26H352a96 96 0 0 1 96 96z"
                ></path>
              </g>
            </svg>
          </span>
          <span
            slot="label"
            class="text-gray-700 text-sm font-semibold rounded"
          >
            15 mai
          </span>
          <span slot="description" class="text-xs"> Step 1 </span>
        </fw-timeline-step>
        <fw-timeline-step completed>
          <span
            slot="icon"
            class="flex items-center justify-center w-10 h-10 bg-blue-600 rounded-full text-white"
          >
            <svg
              height="20"
              width="20"
              aria-hidden="true"
              focusable="false"
              data-prefix="fad"
              data-icon="adjust"
              class="svg-inline--fa fa-adjust fa-w-16"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 512 512"
            >
              <g class="fa-group">
                <path
                  class="fa-secondary"
                  fill="currentColor"
                  d="M504 256c0 137-111 248-248 248V8c137 0 248 111 248 248z"
                  opacity="0.4"
                ></path>
                <path
                  class="fa-primary"
                  fill="currentColor"
                  d="M256 8v496C119 504 8 393 8 256S119 8 256 8z"
                ></path>
              </g>
            </svg>
          </span>
          <span
            slot="label"
            class="text-gray-700 text-sm font-semibold rounded"
          >
            Step 2
          </span>
          <span slot="description" class="text-xs">
            All the Lorem Ipsum generators on the Internet tend to repeat
            predefined chunks as necessary
          </span>
        </fw-timeline-step>
        <fw-timeline-step completed>
          <span
            slot="icon"
            class="flex items-center justify-center w-10 h-10 bg-purple-600 rounded-full text-white"
          >
            <svg
              height="20"
              width="20"
              aria-hidden="true"
              focusable="false"
              data-prefix="fad"
              data-icon="album"
              class="svg-inline--fa fa-album fa-w-14"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 448 512"
            >
              <g class="fa-group">
                <path
                  class="fa-secondary"
                  fill="currentColor"
                  d="M416 32H32A32 32 0 0 0 0 64v384a32 32 0 0 0 32 32h384a32 32 0 0 0 32-32V64a32 32 0 0 0-32-32zM224 416a160 160 0 1 1 160-160 160 160 0 0 1-160 160z"
                  opacity="0.4"
                ></path>
                <path
                  class="fa-primary"
                  fill="currentColor"
                  d="M224 96a160 160 0 1 0 160 160A160 160 0 0 0 224 96zm0 192a32 32 0 1 1 32-32 32 32 0 0 1-32 32z"
                ></path>
              </g>
            </svg>
          </span>
          <span
            slot="label"
            class="text-gray-700 text-sm font-semibold rounded"
          >
            This is a rather long label
          </span>
          <span slot="description" class="text-xs">
            If you are going to use a passage of Lorem Ipsum, you need to be
            sure there isn't anything embarrassing hidden in the middle of text.
          </span>
        </fw-timeline-step>
        <fw-timeline-step>
          <span
            slot="icon"
            class="flex items-center justify-center w-10 h-10 bg-gray-300 rounded-full text-gray-900"
          >
            <svg
              height="20"
              width="20"
              aria-hidden="true"
              focusable="false"
              data-prefix="fad"
              data-icon="bell-on"
              class="svg-inline--fa fa-bell-on fa-w-20"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 640 512"
            >
              <g class="fa-group">
                <path
                  class="fa-secondary"
                  fill="currentColor"
                  d="M479.92,208c0-77.69-54.48-139.91-127.94-155.16V32a32,32,0,1,0-64,0V52.84C214.56,68.09,160.08,130.31,160.08,208c0,102.31-36.14,133.53-55.47,154.28A31.28,31.28,0,0,0,96,384c.11,16.41,13,32,32.09,32H511.91c19.11,0,32-15.59,32.09-32a31.23,31.23,0,0,0-8.61-21.72C516.06,341.53,479.92,310.31,479.92,208Z"
                  opacity="0.4"
                ></path>
                <path
                  class="fa-primary"
                  fill="currentColor"
                  d="M88,168H24a24,24,0,0,0,0,48H88a24,24,0,0,0,0-48ZM131.08,55.22l-55.42-32a24,24,0,1,0-24,41.56l55.42,32a24,24,0,1,0,24-41.56Zm457.26,9.56a24,24,0,0,0-24-41.56l-55.42,32a24,24,0,1,0,24,41.56ZM320,512a64,64,0,0,0,64-64H256A64,64,0,0,0,320,512ZM616,168H552a24,24,0,0,0,0,48h64a24,24,0,0,0,0-48Z"
                ></path>
              </g>
            </svg>
          </span>
          <span
            slot="label"
            class="text-gray-700 text-sm font-semibold rounded"
          >
            Step 4
          </span>
          <span slot="description" class="text-xs">Step 4</span>
        </fw-timeline-step>
      </fw-timeline>
    </div>
  </div>
`;

export const Sandbox = Template.bind({});
Sandbox.args = {
  'step-width': 110,
  'line-stroke-width': 4,
  'line-color': '#ececec',
  'icon-wrapper-size': 40,
};

export const Showcase = () => `
<div class="p-4 pt-10 md:grid md:grid-cols-2 gap-y-20 h-full w-full mt-16">
  <fw-timeline step-width="110" class="mx-auto">
    <fw-timeline-step completed>
      <span slot="icon" class="flex items-center justify-center w-10 h-10 bg-green-300 text-green-700 font-bold rounded-full ">
        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="heart" class="w-4 h-4 svg-inline--fa fa-heart fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M472.1 270.5l-193.1 199.7c-12.64 13.07-33.27 13.08-45.91 .0107l-193.2-199.7C-16.21 212.5-13.1 116.7 49.04 62.86C103.3 15.88 186.4 24.42 236.3 75.98l19.7 20.27l19.7-20.27c49.95-51.56 132.1-60.1 187.3-13.12C525.1 116.6 528.2 212.5 472.1 270.5z"></path></svg>
      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold rounded">
        Mise en favoris
      </span>
      <span slot="description" class="text-xs">
        Vous avez mis cette offre en favoris le 15 mai
      </span>
    </fw-timeline-step>
    <fw-timeline-step completed>
      <span slot="icon" class="flex items-center justify-center w-10 h-10 bg-green-300 text-green-700 font-bold rounded-full ">
      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file-lines" class="h-4 w-4 svg-inline--fa fa-file-lines fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z"></path></svg>      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold rounded">
        Dépot de CV
      </span>
      <span slot="description" class="text-xs">
        Vous avez déposé votre CV aujourd'hui
      </span>
    </fw-timeline-step>
    <fw-timeline-step>
      <span slot="icon" class="flex items-center justify-center w-10 h-10 bg-gray-200 text-gray-600 font-bold rounded-full ">
      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="eye" class="h-4 w-4 svg-inline--fa fa-eye fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M572.5 238.1C518.3 115.5 410.9 32 288 32S57.69 115.6 3.469 238.1C1.563 243.4 0 251 0 256c0 4.977 1.562 12.6 3.469 17.03C57.72 396.5 165.1 480 288 480s230.3-83.58 284.5-206.1C574.4 268.6 576 260.1 576 256C576 251 574.4 243.4 572.5 238.1zM432 256c0 79.45-64.47 144-143.9 144C208.6 400 144 335.5 144 256S208.5 112 288 112S432 176.5 432 256zM288 160C285.7 160 282.4 160.4 279.5 160.8C284.8 170 288 180.6 288 192c0 35.35-28.65 64-64 64C212.6 256 201.1 252.7 192.7 247.5C192.4 250.5 192 253.6 192 256c0 52.1 43 96 96 96s96-42.99 96-95.99S340.1 160 288 160z"></path></svg>      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold rounded">
        CV évalué
      </span>
      <span slot="description" class="text-xs">
        Votre CV est en attente d'évaluation par l'entreprise
      </span>
    </fw-timeline-step>
    <fw-timeline-step>
      <span slot="icon" class="flex items-center justify-center w-10 h-10 bg-gray-200 text-gray-600 font-bold rounded-full ">
      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="golf-flag-hole" class="h-4 w-4 svg-inline--fa fa-golf-flag-hole fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 320c-27.96 0-54.81 1.74-80 4.848v-82.11l198.4-99.21C380.8 140.3 384 134.2 384 128s-3.194-12.31-9.585-15.51L153.1 1.853c-2.539-1.269-5.176-1.853-7.746-1.853C136.2-.0004 128 7.31 128 17.36v315.6C51.55 349.6 0 380.5 0 416c0 53.02 114.6 96 256 96s256-42.98 256-96S397.4 320 256 320zM224 448c-35.35 0-64-14.33-64-32c0-17.67 28.65-32 64-32s64 14.33 64 32C288 433.7 259.3 448 224 448z"></path></svg>      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold rounded">
        Contrat signé
      </span>
      <span slot="description" class="text-xs">
        Vous pourrez signer votre contrat lorsque l'entreprise aura accepté votre candidature
      </span>
    </fw-timeline-step>
  </fw-timeline>

  <fw-timeline icon-wrapper-size="32" class="mx-auto">
    <fw-timeline-step completed>
      <span slot="icon" class="flex items-center justify-center w-8 h-8 bg-gray-200 text-gray-600 font-bold ">
        1
      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold bg-gray-100 px-2 py-1">
        Step 1
      </span>
      <span slot="description">
        Step 1
      </span>
    </fw-timeline-step>
    <fw-timeline-step completed>
      <span slot="icon" class="flex items-center justify-center w-8 h-8 bg-gray-200 text-gray-600 font-bold ">
        2
      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold bg-gray-100 px-2 py-1">
        Step 2
      </span>
      <span slot="description">
        Step 2
      </span>
    </fw-timeline-step>
    <fw-timeline-step completed>
      <span slot="icon" class="flex items-center justify-center w-8 h-8 bg-red-200 text-red-700 font-bold ">
        3
      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold bg-gray-100 px-2 py-1">
        Step 3
      </span>
      <span slot="description">
        Step 3
      </span>
    </fw-timeline-step>
  </fw-timeline>

  <fw-timeline class="mx-auto">
    <fw-timeline-step completed>
      <span slot="icon" class="flex items-center justify-center w-10 h-10 bg-blue-600 rounded-full text-white">
        <svg width="20" height="20" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="acorn" class="svg-inline--fa fa-acorn fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M32 256h384a258.87 258.87 0 0 1-143.11 231.55L224 512l-48.89-24.45A258.87 258.87 0 0 1 32 256z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M448 160v32a32 32 0 0 1-32 32H32a32 32 0 0 1-32-32v-32a96 96 0 0 1 96-96h106a132.41 132.41 0 0 1 29.41-58.64 15.7 15.7 0 0 1 11.31-5.3 15.44 15.44 0 0 1 12 4.72L266 16.1a16 16 0 0 1 .66 21.9 84.32 84.32 0 0 0-15.16 26H352a96 96 0 0 1 96 96z"></path></g></svg>
      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold rounded">
        Step 1
      </span>
      <span slot="description" class="text-xs">
        Ut enim ad minima veniam, quis nostrum exercitationem
      </span>
    </fw-timeline-step>
    <fw-timeline-step completed>
      <span slot="icon" class="flex items-center justify-center w-10 h-10 bg-blue-600 rounded-full text-white">
        <svg height="20" width="20" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="adjust" class="svg-inline--fa fa-adjust fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M504 256c0 137-111 248-248 248V8c137 0 248 111 248 248z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M256 8v496C119 504 8 393 8 256S119 8 256 8z"></path></g></svg>
      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold rounded">
        Step 2
      </span>
      <span slot="description" class="text-xs">
        Ut enim ad minima veniam, quis nostrum exercitationem
      </span>
    </fw-timeline-step>
    <fw-timeline-step completed>
      <span slot="icon" class="flex items-center justify-center w-10 h-10 bg-purple-600 rounded-full text-white">
        <svg height="20" width="20" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="album" class="svg-inline--fa fa-album fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M416 32H32A32 32 0 0 0 0 64v384a32 32 0 0 0 32 32h384a32 32 0 0 0 32-32V64a32 32 0 0 0-32-32zM224 416a160 160 0 1 1 160-160 160 160 0 0 1-160 160z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M224 96a160 160 0 1 0 160 160A160 160 0 0 0 224 96zm0 192a32 32 0 1 1 32-32 32 32 0 0 1-32 32z"></path></g></svg>
      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold rounded">
        This is a rather long label
      </span>
      <span slot="description" class="text-xs">
        Ut enim ad minima veniam, quis nostrum exercitationem
      </span>
    </fw-timeline-step>
    <fw-timeline-step>
      <span slot="icon" class="flex items-center justify-center w-10 h-10 bg-gray-300 rounded-full text-white">
        <svg height="20" width="20" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="bell-on" class="svg-inline--fa fa-bell-on fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M479.92,208c0-77.69-54.48-139.91-127.94-155.16V32a32,32,0,1,0-64,0V52.84C214.56,68.09,160.08,130.31,160.08,208c0,102.31-36.14,133.53-55.47,154.28A31.28,31.28,0,0,0,96,384c.11,16.41,13,32,32.09,32H511.91c19.11,0,32-15.59,32.09-32a31.23,31.23,0,0,0-8.61-21.72C516.06,341.53,479.92,310.31,479.92,208Z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M88,168H24a24,24,0,0,0,0,48H88a24,24,0,0,0,0-48ZM131.08,55.22l-55.42-32a24,24,0,1,0-24,41.56l55.42,32a24,24,0,1,0,24-41.56Zm457.26,9.56a24,24,0,0,0-24-41.56l-55.42,32a24,24,0,1,0,24,41.56ZM320,512a64,64,0,0,0,64-64H256A64,64,0,0,0,320,512ZM616,168H552a24,24,0,0,0,0,48h64a24,24,0,0,0,0-48Z"></path></g></svg>
      </span>
      <span slot="label" class="text-gray-700 text-sm font-semibold rounded">
        Step 4
      </span>
      <span slot="description" class="text-xs">Step 4</span>
    </fw-timeline-step>
  </fw-timeline>
</div>
`;
