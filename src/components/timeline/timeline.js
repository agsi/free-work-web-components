import { LitElement, html } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { styleMap } from 'lit/directives/style-map.js';
import { timelineStyles, timelineStepStyles } from './timeline.styles';

class Timeline extends LitElement {
  static get properties() {
    return {
      'step-width': {
        type: Number,
        reflect: true,
      },

      'line-stroke-width': {
        type: Number,
        reflect: true,
      },

      'line-color': {
        type: String,
        reflect: true,
      },

      'icon-wrapper-size': {
        type: Number,
        reflect: true,
      },
    };
  }

  constructor() {
    super();
  }

  static get styles() {
    return timelineStyles;
  }

  render() {
    const styles = {
      '--timeline-step-width': this['step-width']
        ? this['step-width'] + 'px'
        : null,
      '--timeline-line-stroke-width': this['line-stroke-width']
        ? this['line-stroke-width'] + 'px'
        : null,
      '--timeline-line-color': this['line-color'] ? this['line-color'] : null,
      '--timeline-icon-wrapper-height': this['icon-wrapper-size']
        ? this['icon-wrapper-size'] + 'px'
        : null,
      '--timeline-icon-wrapper-width': this['icon-wrapper-size']
        ? this['icon-wrapper-size'] + 'px'
        : null,
    };

    return html`<div style="${styleMap(styles)}" class="timeline">
      <div class="line"></div>
      <slot></slot>
    </div>`;
  }
}

customElements.define('fw-timeline', Timeline);

class TimelineItem extends LitElement {
  static get properties() {
    return {
      completed: {
        type: Boolean,
        reflect: true,
      },
    };
  }

  constructor() {
    super();
    this.completed = false;
    this.uniqueId = `tooltip-${(Math.random() + 1).toString(36).substring(7)}`;
    this._onResize = this._onResize.bind(this);
    this._isSmallMedia = this._isSmallMedia.bind(this);
  }

  static get styles() {
    return timelineStepStyles;
  }

  _isSmallMedia() {
    return window.innerWidth < 800;
  }

  firstUpdated() {
    window.addEventListener('resize', this._onResize);
  }

  disconnectedCallback() {
    window.removeEventListener('resize', this._onResize);
  }

  _onResize() {
    this.requestUpdate();
  }

  render() {
    const classes = {
      'timeline-step': true,
      completed: this.completed,
    };

    const description = html`<slot name="description" slot="content"></slot>`;

    const icon = html`
      <div id="${this.uniqueId}" class="timeline-step-icon">
        <div class="timeline-step-icon-wrapper">
          <slot name="icon"></slot>
        </div>
      </div>
    `;

    const tooltip = html`${icon}
      <fw-tooltip target="#${this.uniqueId}">
        <slot name="description"></slot>
      </fw-tooltip>`;

    return html`<div class="${classMap(classes)}">
      ${this._isSmallMedia() ? icon : tooltip}
      <div class="timeline-content">
        <div class="timeline-step-label"><slot name="label"></div>
        ${this._isSmallMedia() ? description : null}
      </div>
    </div>`;
  }
}

customElements.define('fw-timeline-step', TimelineItem);
