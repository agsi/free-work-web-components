import { create } from '@storybook/theming/create';
import { addons } from '@storybook/addons';

addons.setConfig({
  theme: create({
    base: 'light',

    brandTitle: 'Free-Work — Web Components',
    brandUrl: 'https://www.freelance-info.fr/',
    brandImage:
      'https://www.freelance-info.fr/freelance-info/img/i/android-icon-192x192.png',
  }),
});
